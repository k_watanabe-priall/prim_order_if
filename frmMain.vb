﻿Imports System
Imports System.IO
Imports System.Runtime.Serialization
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Windows.Forms
Imports Microsoft.Win32

Public Class frmMain

#Region "Information"
    '******************************************************************************************
    '* 湘南第一病院 様 向け オーダXMLデータ登録アプリケーション
    '* ---------------------機能概要--------------------
    '* オーダ情報(XMLデータ)取得アプリケーションにて、本アプリケーションの処理フォルダ内に
    '* オーダ情報XMLが生成される。
    '* 処理フォルダを一定時間毎に監視し、XMLファイルを検知したら自DBにXML内容を登録する。
    '******************************************************************************************
#End Region

    Private objOrder As New typeOrderData

#Region "Form Events"

#Region "Form Load"
    Private Sub frmMain_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If My.Settings.MAIN_SV_DIV = True Then
            Me.Text &= "(メインサーバ) " & My.Settings.APP_VERSION
        Else
            Me.Text &= "(待機サーバ) " & My.Settings.APP_VERSION
        End If

        Call subInitForm()
    End Sub
#End Region

#End Region

#Region "Sub Routine"

#Region "メイン(subProcMain)"
    '***************************************************************************
    '* 関数名称：メイン
    '* 機能概要：subProcMain
    '* 引　数　：無
    '* 戻り値　：無
    '* 作成日付：2014.02.06
    '* 作成者　：Created By Watanabe
    '***************************************************************************
    Private Sub subProcMain()
        Dim lngCounter As Long = 0
        Dim lngFileCount As Long = 0
        Dim strMsg As String = vbNullString
        Dim intRet As Integer = 0
        Dim strOrderAno As String = vbNullString
        Dim strPatientAno As String = vbNullString

        '///処理フォルダ内ファイル名取得
        Dim strFileNames As String() = System.IO.Directory.GetFiles( _
                              My.Settings.WORK_PATH, "*.*", System.IO.SearchOption.TopDirectoryOnly)

        '///取得ファイル名が0件の場合、処理を抜ける
        If strFileNames.Length = 0 Then Exit Sub

        '///DBConnect
        Call fncDBConnect(strMsg)

        '///取得ファイル数退避
        lngFileCount = strFileNames.Length

        For lngCounter = 0 To lngFileCount - 1
            Dim strFileNM As String = System.IO.Path.GetFileName(strFileNames(lngCounter))
            Call subOutLog("****************************************************************", 0)
            Call subDispMsg(0, "****************************************************************", strMsg)
            Call subOutLog("メイン：XMLファイル検知(" & lngCounter + 1 & "/" & lngFileCount & ")", 0)
            Call subDispMsg(0, "メイン：XMLファイル検知(" & lngCounter + 1 & "/" & lngFileCount & ")", strMsg)
            '---処理ファイル名ログ出力追加 ADD By Watanabe 2014.08.06
            Call subOutLog("メイン：XMLファイル名(" & strFileNM & ")", 0)
            Call subDispMsg(0, "メイン：XMLファイル名(" & strFileNM & ")", strMsg)

            '///XML読込
            Call subReadXML(strFileNames(lngCounter))
            '---取得データのログ出力を追加 ADD By Watanabe 2014.08.06
            'Call subOutLog("メイン：取得オーダ番号 " & objOrder.オーダデータ.オーダ番号, 0)
            'Call subDispMsg(0, "メイン：取得オーダ番号 " & objOrder.オーダデータ.オーダ番号, strMsg)

            '2016/03/09 add 構造体内のコーテーションを削除
            Call subAdjustmentXML()

            Call subOutLog("メイン：患者番号 " & objOrder.オーダデータ.患者番号, 0)
            Call subDispMsg(0, "メイン：患者番号 " & objOrder.オーダデータ.患者番号, strMsg)
            Call subOutLog("メイン：シノニム " & objOrder.オーダデータ.シノニム, 0)
            Call subDispMsg(0, "メイン：シノニム " & objOrder.オーダデータ.シノニム, strMsg)
            Call subOutLog("メイン：区切番号 " & objOrder.オーダデータ.区切番号, 0)
            Call subDispMsg(0, "メイン：区切番号 " & objOrder.オーダデータ.区切番号, strMsg)
            Call subOutLog("メイン：診療区分 " & objOrder.オーダデータ.診療区分, 0)
            Call subDispMsg(0, "メイン：診療区分 " & objOrder.オーダデータ.診療区分, strMsg)
            Call subOutLog("メイン：オーダステータス " & objOrder.オーダデータ.オーダステータス, 0)
            Call subDispMsg(0, "メイン：オーダステータス " & objOrder.オーダデータ.オーダステータス, strMsg)

            Select Case objOrder.オーダデータ.オーダステータス
                Case "0", "1", "3", "4"
                    '///処理継続条件追加 ADD By Watanabe 2014.07.22
                    If objOrder.オーダデータ.オーダステータス = "3" Then
                        If objOrder.オーダデータ.診療区分 <> "3" Then
                            Call subOutLog("処理対象外オーダステータス:" & objOrder.オーダデータ.オーダステータス, 0)
                            Call subDispMsg(0, "処理対象外オーダステータス:" & objOrder.オーダデータ.オーダステータス, strMsg)
                            '///処理済みファイルをOKフォルダに移動
                            Call subMoveFile(strFileNM, strMsg, 0)

                            Call subOutLog("****************************************************************", 0)
                            Call subDispMsg(0, "****************************************************************", strMsg)
                            Exit Sub
                        End If
                    End If

                    '///患者情報存在チェック
                    intRet = fncChkPatient(objOrder.オーダデータ.患者番号, strPatientAno, strMsg)
                    Select Case intRet
                        Case RET_NOTFOUND
                            '///患者情報新規登録
                            Call subInsPatient(0, strMsg)
                            '///患者障害情報新規登録
                            Call subInsPatientShogai(strMsg)
                            '///患者常用薬情報新規登録
                            Call subInsPatientJoyoyaku(strMsg)
                            '///患者感染症情報新規登録
                            Call subInsPatientKansen(strMsg)
                        Case RET_NORMAL
                            '///患者情報更新処理
                            Call subInsPatient(1, strMsg)
                            '///患者障害情報新規登録
                            Call subInsPatientShogai(strMsg)
                            '///患者常用薬情報新規登録
                            Call subInsPatientJoyoyaku(strMsg)
                            '///患者感染症情報新規登録
                            Call subInsPatientKansen(strMsg)
                        Case RET_ERROR
                            '///エラー時はログ出力し次のファイルを処理する
                            Call subDispMsg(1, "患者情報存在チェック(subProcMain):エラー " & strMsg, strMsg)
                            '///処理済みファイルをERRフォルダに移動
                            Call subMoveFile(strFileNM, strMsg, 1)

                            Exit For
                    End Select

                    '///データ存在チェック
                    intRet = fncChkOrder(objOrder.オーダデータ.オーダ番号, strOrderAno, strMsg)
                    Select Case intRet
                        Case RET_NOTFOUND
                            '///オーダ新規登録
                            Call subInsOrder(0, strMsg)
                            '///オーダ明細情報新規登録
                            Call subInsOrderMeisai(strMsg)
                            '///オーダ詳細情報新規登録
                            Call subInsOrderShosai(strMsg)
                        Case RET_NORMAL
                            '///更新処理
                            Call subInsOrder(1, strMsg)

                            If objOrder.オーダデータ.オーダステータス <> "4" Then
                                '///オーダ明細情報新規登録
                                Call subInsOrderMeisai(strMsg)
                                '///オーダ詳細情報新規登録
                                Call subInsOrderShosai(strMsg)
                            End If
                        Case RET_ERROR
                            Call subDispMsg(1, "データ存在チェック(subProcMain):エラー " & strMsg, strMsg)
                            '///処理済みファイルをERRフォルダに移動
                            Call subMoveFile(strFileNM, strMsg, 1)
                            Exit For
                    End Select

                    '---ここではPATIENT_ANOの更新は行わない Deleted By Watanabe 2014.07.24
                    '///登録患者情報の患者登録番号をオーダテーブルに反映
                    '                   Call subUpdOrderPatientAno(strPatientAno, objOrder.オーダデータ.患者番号, strOrderAno, strMsg)
                Case Else
                    Call subOutLog("処理対象外オーダステータス:" & objOrder.オーダデータ.オーダステータス, 0)
                    Call subDispMsg(0, "処理対象外オーダステータス:" & objOrder.オーダデータ.オーダステータス, strMsg)
            End Select
            '///処理済みファイルをOKフォルダに移動
            Call subMoveFile(strFileNM, strMsg, 0)

            Call subOutLog("****************************************************************", 0)
            Call subDispMsg(0, "****************************************************************", strMsg)
        Next lngCounter

        '///DB DisConnect
        Call fncDBDisConnect(strMsg)
    End Sub
#End Region

#Region "XML読込処理(subReadXML)"
    '***************************************************************************
    '* 関数名称：XML読込処理
    '* 機能概要：subReadXML
    '* 引　数　：無
    '* 戻り値　：無
    '* 作成日付：2014.02.05
    '* 作成者　：Created By Watanabe
    '***************************************************************************
    Private Sub subReadXML(ByVal strFileNM As String)

        Dim fs As New FileStream( _
            strFileNM, FileMode.Open, FileAccess.Read)

        Dim serializer As System.Xml.Serialization.XmlSerializer = New System.Xml.Serialization.XmlSerializer(GetType(typeOrderData))
        Dim ns As Xml.Serialization.XmlSerializerNamespaces = New Xml.Serialization.XmlSerializerNamespaces
        '読み込んで逆シリアル化する
        objOrder = New typeOrderData        '初期化を追加 ADD By Watanabe 2014.07.09
        objOrder = serializer.Deserialize(fs)

        fs.Close()
    End Sub
#End Region

#Region "XML読込後不要文字削除(subAdjustmentXML)"
    '***************************************************************************
    '* 関数名称：XML読込後不要文字削除
    '* 機能概要：subAdjustmentXML
    '* 引　数　：無
    '* 戻り値　：無
    '* 作成日付：2016.03.09
    '* 作成者　：Created By nagata
    '***************************************************************************
    Private Sub subAdjustmentXML()
        Dim strMsg As String

        strMsg = fncAdjustmentPatientInfo()
        If strMsg <> "" Then
            Call subOutLog(strMsg, 1)
        End If
        strMsg = fncAdjustmentOrderData()
        If strMsg <> "" Then
            Call subOutLog(strMsg, 1)
        End If
        strMsg = fncAdjustmentHeader()
        If strMsg <> "" Then
            Call subOutLog(strMsg, 1)
        End If
        strMsg = fncAdjustmentInOut()
        If strMsg <> "" Then
            Call subOutLog(strMsg, 1)
        End If

    End Sub
    Private Function fncAdjustmentString(ByVal str As String) As String
        If str = "" Then
            fncadjustmentString = ""
        Else
            fncadjustmentString = Replace(Replace(str, """", ""), "'", "")
        End If
    End Function
    Private Function fncAdjustmentPatientInfo() As String
        Try
            fncAdjustmentPatientInfo = ""
            With objOrder.患者情報
                .患者番号 = fncadjustmentstring(.患者番号)
                .漢字氏名 = fncadjustmentstring(.漢字氏名)
                .カナ氏名 = fncadjustmentstring(.カナ氏名)
                .性別 = fncadjustmentstring(.性別)
                .性別名称 = fncadjustmentstring(.性別名称)
                .住所 = fncadjustmentstring(.住所)
                .身長 = fncadjustmentstring(.身長)
                .体重 = fncadjustmentstring(.体重)
                .生年月日 = fncadjustmentstring(.生年月日)
                .郵便番号 = fncadjustmentstring(.郵便番号)
                .電話番号 = fncadjustmentstring(.電話番号)
                .連絡先電話番号 = fncadjustmentstring(.連絡先電話番号)
                .旧カナ氏名 = fncadjustmentstring(.旧カナ氏名)
                .旧漢字氏名 = fncadjustmentstring(.旧漢字氏名)
                .旧氏名有効期限 = fncadjustmentstring(.旧氏名有効期限)
                .血液型ABO = fncadjustmentstring(.血液型ABO)
                .血液型RH = fncadjustmentstring(.血液型RH)
                .血液型最終確認日 = fncadjustmentstring(.血液型最終確認日)
                .妊娠有無 = fncadjustmentstring(.妊娠有無)
                .妊娠有無名称 = fncadjustmentstring(.妊娠有無名称)
                .分娩予定日 = fncadjustmentstring(.分娩予定日)
                .妊娠週数 = fncadjustmentstring(.妊娠週数)
                .歩行状態 = fncadjustmentstring(.歩行状態)
                .歩行状態名称 = fncadjustmentstring(.歩行状態名称)
                .安静度 = fncadjustmentstring(.安静度)
                .安静度名称 = fncadjustmentstring(.安静度名称)
                .看護度 = fncadjustmentstring(.看護度)
                .食事介助度 = fncadjustmentstring(.食事介助度)
                .食事介助度名称 = fncadjustmentstring(.食事介助度名称)
                .服薬指導 = fncadjustmentstring(.服薬指導)
                .服薬指導名称 = fncadjustmentstring(.服薬指導名称)
                .任意情報 = fncadjustmentstring(.任意情報)
                .任意情報名称 = fncadjustmentstring(.任意情報名称)

                Dim i As Long
                For i = 0 To .障害情報.Length - 1
                    .障害情報(i).障害情報番号 = fncadjustmentstring(.障害情報(i).障害情報番号)
                    .障害情報(i).障害情報名称 = fncadjustmentstring(.障害情報(i).障害情報名称)
                    .障害情報(i).障害情報区分 = fncadjustmentstring(.障害情報(i).障害情報区分)
                    .障害情報(i).障害情報区分名称 = fncadjustmentstring(.障害情報(i).障害情報区分名称)
                Next
                For i = 0 To .常用薬.Length - 1
                    .常用薬(i).常用薬番号 = fncadjustmentstring(.常用薬(i).常用薬番号)
                    .常用薬(i).常用薬名称 = fncadjustmentstring(.常用薬(i).常用薬名称)
                    .常用薬(i).常用薬区分 = fncadjustmentstring(.常用薬(i).常用薬区分)
                    .常用薬(i).常用薬区分名称 = fncadjustmentstring(.常用薬(i).常用薬区分名称)
                Next
                For i = 0 To .感染症.Length - 1
                    .感染症(i).感染症番号 = fncadjustmentstring(.感染症(i).感染症番号)
                    .感染症(i).感染症名称 = fncadjustmentstring(.感染症(i).感染症名称)
                    .感染症(i).感染症区分 = fncadjustmentstring(.感染症(i).感染症区分)
                    .感染症(i).感染症区分名称 = fncadjustmentstring(.感染症(i).感染症区分名称)
                Next

            End With
            Exit Function
        Catch ex As Exception
            fncAdjustmentPatientInfo = "患者情報不要文字列削除にて失敗しました。"
            Exit Function
        End Try

    End Function
    Private Function fncAdjustmentOrderData() As String
        Try
            fncAdjustmentOrderData = ""
            With objOrder.オーダデータ
                .患者番号 = fncAdjustmentString(.患者番号)
                .指示日 = fncadjustmentstring(.指示日)
                .シノニム = fncadjustmentstring(.シノニム)
                .診療区分 = fncadjustmentstring(.診療区分)
                .区切番号 = fncadjustmentstring(.区切番号)
                .診療区分名称 = fncadjustmentstring(.診療区分名称)
                .診療区分略称 = fncadjustmentstring(.診療区分略称)
                .緊急区分 = fncadjustmentstring(.緊急区分)
                .緊急区分名称 = fncadjustmentstring(.緊急区分名称)
                .事後入力フラグ = fncadjustmentstring(.事後入力フラグ)
                .事後入力フラグ名称 = fncadjustmentstring(.事後入力フラグ名称)
                .保険種別 = fncadjustmentstring(.保険種別)
                .保険種別名称 = fncadjustmentstring(.保険種別名称)
                .新規フラグ = fncadjustmentstring(.新規フラグ)
                .新規フラグ名称 = fncadjustmentstring(.新規フラグ名称)
                .検査種別 = fncadjustmentstring(.検査種別)
                .検査種別名称 = fncadjustmentstring(.検査種別名称)
                .処方区分 = fncadjustmentstring(.処方区分)
                .処方区分名称 = fncadjustmentstring(.処方区分名称)
                .定臨時フラグ = fncadjustmentstring(.定臨時フラグ)
                .定臨時フラグ名称 = fncadjustmentstring(.定臨時フラグ名称)
                .日回数 = fncadjustmentstring(.日回数)
                .実施予定開始日時 = fncadjustmentstring(.実施予定開始日時)
                .実施予定開始時間区分 = fncadjustmentstring(.実施予定開始時間区分)
                .実施予定開始時間区分名称 = fncadjustmentstring(.実施予定開始時間区分名称)
                .実施予定日間隔 = fncadjustmentstring(.実施予定日間隔)
                .実施予定終了日付 = fncadjustmentstring(.実施予定終了日付)
                .実施予定終了時間区分 = fncadjustmentstring(.実施予定終了時間区分)
                .実施予定終了時間区分名称 = fncadjustmentstring(.実施予定終了時間区分名称)
                .オーダ番号 = fncadjustmentstring(.オーダ番号)
                .中止判別フラグ = fncadjustmentstring(.中止判別フラグ)
                .中止日時 = fncadjustmentstring(.中止日時)
                .中止時間区分 = fncadjustmentstring(.中止時間区分)
                .中止時間区分名称 = fncadjustmentstring(.中止時間区分名称)
                .部門科コード = fncadjustmentstring(.部門科コード)
                .部門科名称 = fncadjustmentstring(.部門科名称)
                .フリーコメント = fncadjustmentstring(.フリーコメント)
                .オーダステータス = fncadjustmentstring(.オーダステータス)
                .不実施情報日時 = fncadjustmentstring(.不実施情報日時)
                .受付日時 = fncadjustmentstring(.受付日時)
                .実施日時 = fncadjustmentstring(.実施日時)
                .削除日時 = fncadjustmentstring(.削除日時)
                .中止操作日時 = fncadjustmentstring(.中止操作日時)
                .補足入力日時 = fncadjustmentstring(.補足入力日時)
                .検査結果有無 = fncadjustmentstring(.検査結果有無)
                .検査結果有無名称 = fncadjustmentstring(.検査結果有無名称)
                .食事摂取要否 = fncadjustmentstring(.食事摂取要否)
                .食事摂取要否名称 = fncadjustmentstring(.食事摂取要否名称)
                .検査レポート有無 = fncadjustmentstring(.検査レポート有無)
                .検査レポート有無名称 = fncadjustmentstring(.検査レポート有無名称)
                .承認要否 = fncadjustmentstring(.承認要否)
                .承認要否名称 = fncadjustmentstring(.承認要否名称)
                .公費情報連番1 = fncadjustmentstring(.公費情報連番1)
                .公費情報連番1名称 = fncadjustmentstring(.公費情報連番1名称)
                .公費情報連番2 = fncadjustmentstring(.公費情報連番2)
                .公費情報連番2名称 = fncadjustmentstring(.公費情報連番2名称)

                .不実施情報操作者.職員情報.職員ID = fncadjustmentstring(.不実施情報操作者.職員情報.職員ID)
                .不実施情報操作者.職員情報.職員氏名 = fncadjustmentstring(.不実施情報操作者.職員情報.職員氏名)

                .受付操作者.職員情報.職員ID = fncadjustmentstring(.受付操作者.職員情報.職員ID)
                .受付操作者.職員情報.職員氏名 = fncadjustmentstring(.受付操作者.職員情報.職員氏名)

                .実施操作者.職員情報.職員ID = fncadjustmentstring(.実施操作者.職員情報.職員ID)
                .実施操作者.職員情報.職員氏名 = fncadjustmentstring(.実施操作者.職員情報.職員氏名)

                .削除操作者.職員情報.職員ID = fncadjustmentstring(.削除操作者.職員情報.職員ID)
                .削除操作者.職員情報.職員氏名 = fncadjustmentstring(.削除操作者.職員情報.職員氏名)

                .中止操作者.職員情報.職員ID = fncadjustmentstring(.中止操作者.職員情報.職員ID)
                .中止操作者.職員情報.職員氏名 = fncadjustmentstring(.中止操作者.職員情報.職員氏名)

                .中止指示医.職員情報.職員ID = fncadjustmentstring(.中止指示医.職員情報.職員ID)
                .中止指示医.職員情報.職員氏名 = fncadjustmentstring(.中止指示医.職員情報.職員氏名)

                .補足入力操作者.職員情報.職員ID = fncadjustmentstring(.補足入力操作者.職員情報.職員ID)
                .補足入力操作者.職員情報.職員氏名 = fncadjustmentstring(.補足入力操作者.職員情報.職員氏名)

                .管理情報.入外区分 = fncadjustmentstring(.管理情報.入外区分)
                .管理情報.入外区分名称 = fncadjustmentstring(.管理情報.入外区分名称)
                .管理情報.診療科コード = fncadjustmentstring(.管理情報.診療科コード)
                .管理情報.診療科名称 = fncadjustmentstring(.管理情報.診療科名称)
                .管理情報.部門コード = fncadjustmentstring(.管理情報.部門コード)
                .管理情報.部門名称 = fncadjustmentstring(.管理情報.部門名称)
                .管理情報.診察室番号 = fncadjustmentstring(.管理情報.診察室番号)
                .管理情報.診察室名称 = fncadjustmentstring(.管理情報.診察室名称)
                .管理情報.操作者.職員情報.職員ID = fncadjustmentstring(.管理情報.操作者.職員情報.職員ID)
                .管理情報.操作者.職員情報.職員氏名 = fncadjustmentstring(.管理情報.操作者.職員情報.職員氏名)
                .管理情報.指示医.職員情報.職員ID = fncadjustmentstring(.管理情報.指示医.職員情報.職員ID)
                .管理情報.指示医.職員情報.職員氏名 = fncadjustmentstring(.管理情報.指示医.職員情報.職員氏名)

                Dim i As Long
                Dim j As Long
                For i = 0 To .明細.Length - 1
                    .明細(i).明細番号 = fncadjustmentstring(.明細(i).明細番号)
                    .明細(i).入力区分 = fncAdjustmentString(.明細(i).入力区分)
                    .明細(i).入力区分名称 = fncadjustmentstring(.明細(i).入力区分名称)
                    .明細(i).項目品番 = fncAdjustmentString(.明細(i).項目品番)
                    .明細(i).項目名称 = fncAdjustmentString(.明細(i).項目名称)
                    .明細(i).数量 = fncadjustmentstring(.明細(i).数量)
                    .明細(i).単位 = fncAdjustmentString(.明細(i).単位)
                    .明細(i).指示コメント = fncadjustmentstring(.明細(i).指示コメント)
                Next

                .詳細.シェーマコメント = fncadjustmentstring(.詳細.シェーマコメント)

                If .詳細.フレーム Is Nothing Then Exit Function

                For i = 0 To .詳細.フレーム.Length - 1
                    .詳細.フレーム(i).フレーム番号 = fncAdjustmentString(.詳細.フレーム(i).フレーム番号)
                    .詳細.フレーム(i).フレーム名称 = fncadjustmentstring(.詳細.フレーム(i).フレーム名称)
                    For j = 0 To .詳細.フレーム(i).詳細項目.Length - 1
                        .詳細.フレーム(i).詳細項目(j).項目番号 = fncadjustmentstring(.詳細.フレーム(i).詳細項目(j).項目番号)
                        .詳細.フレーム(i).詳細項目(j).項目名称 = fncadjustmentstring(.詳細.フレーム(i).詳細項目(j).項目名称)
                        .詳細.フレーム(i).詳細項目(j).登録内容 = fncadjustmentstring(.詳細.フレーム(i).詳細項目(j).登録内容)
                    Next
                Next
            End With
        Catch ex As Exception
            fncAdjustmentOrderData = "オーダーデータ不要文字列削除にて失敗しました。"
        End Try
    End Function

    Private Function fncAdjustmentHeader() As String
        Try
            fncAdjustmentHeader = ""
            With objOrder.ヘッダ
                .フォーマット名 = fncadjustmentstring(.フォーマット名)
                .メジャーバージョン = fncadjustmentstring(.メジャーバージョン)
                .マイナーバージョン = fncadjustmentstring(.マイナーバージョン)
                .送信元 = fncadjustmentstring(.送信元)
                .送信先 = fncadjustmentstring(.送信先)
            End With
        Catch ex As Exception
            fncAdjustmentHeader = "ヘッダデータ不要文字列削除にて失敗しました。"
        End Try
    End Function

    Private Function fncAdjustmentInOut() As String
        Try
            fncAdjustmentInOut = ""
            With objOrder.入退院情報
                .入院日 = fncadjustmentstring(.入院日)
                .退院日 = fncadjustmentstring(.退院日)
                .病棟コード = fncadjustmentstring(.病棟コード)
                .病棟名称 = fncadjustmentstring(.病棟名称)
                .病室番号 = fncadjustmentstring(.病室番号)
                .病室名称 = fncadjustmentstring(.病室名称)
                .入院科 = fncadjustmentstring(.入院科)
                .入院科名称 = fncadjustmentstring(.入院科名称)
                .最終転棟転室日 = fncadjustmentstring(.最終転棟転室日)
                .主治医.職員ID = fncadjustmentstring(.主治医.職員ID)
                .主治医.職員氏名 = fncadjustmentstring(.主治医.職員氏名)
            End With
        Catch ex As Exception
            fncAdjustmentInOut = "入退院情報不要文字列削除にて失敗しました。"
        End Try
    End Function


#End Region

#Region "処理状況を画面に表示(subDispMsg)"
    '**********************************************************************************
    '* 機能概要　：処理状況を画面に表示
    '* 関数名称　：subDispMsg
    '* 引　数　　：intProc 　=　0・・・情報、1=エラー、2=ワーニング
    '* 　　　　　：strOutMsg =表示メッセージ
    '* 　　　　　：strMsg    =エラー内容退避用
    '* 作成日付　：2014.02.05
    '* 作成者　　：Created By Watanabe
    '* 更新履歴　：
    '**********************************************************************************
    Private Sub subDispMsg(ByVal intProc As Integer, ByVal strOutMsg As String, ByRef strMsg As String)
        Dim strWork As String = vbNullString

        Select Case intProc
            Case 0          '情報
                strWork = "[INF]"
            Case 1          'エラー情報
                strWork = "[ERR]"
            Case 2          'ワーニング情報
                strWork = "[WAR]"
        End Select

        strWork = "( " & Date.Now.ToString & ")" & strWork

        If Me.lstMsg.Items.Count > 256 Then
            Me.lstMsg.Items.Clear()
        End If

        Me.lstMsg.Items.Add(strWork & Space(3) & strOutMsg)
        Me.lstMsg.SelectedIndex = Me.lstMsg.Items.Count - 1
        Me.lstMsg.Refresh()
        Me.Refresh()
    End Sub
#End Region

#Region "オーダ情報系"

#Region "オーダ情報存在チェック(fncChkOrder)"
    '**********************************************************************************
    '* 機能概要　：DB存在チェック
    '* 関数名称　：fncChkOrder
    '* 引　数　　：strMsg    =エラー内容退避用
    '* 戻り値　　：Integer・・・RET_NORMAL(0)=正常、RET_NOTFOUND(-1)、
    '* 　　　　　：       　　　RET_WARNING(-2)=警告、RET_ERROR(-9)=異常
    '* 作成日付　：2014.02.05
    '* 作成者　　：Created By Watanabe
    '* 更新履歴　：
    '**********************************************************************************
    Private Function fncChkOrder(ByVal strOrderNo As String, ByRef strOrderAno As String, ByRef strMsg As String) As Integer
        Dim strSQL As New System.Text.StringBuilder     'SQL文編集用
        Dim dsData As New DataSet                       '取得データ退避用

        Try
            Call subOutLog("オーダ情報存在チェック(fncChkOrder)：Start", 0)
            Call subDispMsg(0, "オーダ情報存在チェック(fncChkOrder)：Start", strMsg)

            '///SQL文編集
            strSQL.Clear()
            strSQL.AppendLine("SELECT isnull(MAX(ORDER_ANO),0) AS MAX_ORDER_ANO ")
            strSQL.AppendLine("FROM TRAN_ORDER ")
            'strSQL.AppendLine("Where ORDER_NO = '" & strOrderNo & "'")
            strSQL.AppendLine("Where ")
            '指示日、患者番号、実施予定開始日時、シノニム、区切番号、診療区分を条件に追加 ADD By Watanabe 2014.07.22
            strSQL.AppendLine("CONVERT(varchar,SHIJI_DATE,112) = '" & objOrder.オーダデータ.指示日.Replace("/", "") & "' ")
            strSQL.AppendLine("AND ")
            strSQL.AppendLine("PATIENT_ID = '" & objOrder.オーダデータ.患者番号 & "' ")
            strSQL.AppendLine("AND ")
            strSQL.AppendLine("CONVERT(varchar,JISSHIYOTEI_KAISHI_DATETIME,112) = '" & objOrder.オーダデータ.実施予定開始日時.Substring(0, 10).Replace("/", "") & "'")
            strSQL.AppendLine("AND ")
            strSQL.AppendLine("SNONYM = '" & objOrder.オーダデータ.シノニム & "' ")
            strSQL.AppendLine("AND ")
            strSQL.AppendLine("KUGIRI_NO = '" & objOrder.オーダデータ.区切番号 & "' ")
            strSQL.AppendLine("AND ")
            strSQL.AppendLine("SHINRYO_KBN = '" & objOrder.オーダデータ.診療区分 & "' ")
            '---DEL_FLGを条件に追加 ADD By Watanabe 2014.08.05
            strSQL.AppendLine("AND ")
            strSQL.AppendLine("DEL_FLG = '0' ")

            '///SQL発行
            dsData = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)

            Call subOutLog("オーダ情報存在チェック(fncChkOrder) SQL：" & strSQL.ToString, 0)

            '///存在データ無
            If dsData.Tables.Count = 0 Then Return RET_NOTFOUND
            If dsData.Tables(0).Rows.Count = 0 Then Return RET_NOTFOUND
            If dsData.Tables(0).Rows(0).Item("MAX_ORDER_ANO") = 0 Then Return RET_NOTFOUND
            strOrderAno = dsData.Tables(0).Rows(0).Item("MAX_ORDER_ANO")

            Call subOutLog("オーダ情報存在チェック(fncChkOrder)：正常終了", 0)
            Call subDispMsg(0, "オーダ情報存在チェック(fncChkOrder)：正常終了", strMsg)

            Return RET_NORMAL

        Catch ex As Exception
            strMsg = ex.Message
            Call subOutLog("オーダ情報存在チェック(fncChkOrder) エラー：" & strMsg, 1)
            Call subDispMsg(1, "オーダ情報存在チェック(fncChkOrder) エラー：", strMsg)

            Return RET_ERROR

        Finally
            '///終了処理
            strSQL.Clear()
            strSQL = Nothing
            dsData.Dispose()
            dsData = Nothing
        End Try
    End Function
#End Region

#Region "オーダ情報登録処理(subInsOrder)"
    '**********************************************************************************
    '* 機能概要　：オーダ情報登録処理
    '* 関数名称　：subInsOrder
    '* 引　数　　：strMsg    =エラー内容退避用
    '* 作成日付　：2014.02.05
    '* 作成者　　：Created By Watanabe
    '* 更新履歴　：
    '**********************************************************************************
    Private Sub subInsOrder(ByVal intProc As Integer, ByRef strMsg As String)
        Dim strSQL As New System.Text.StringBuilder
        Dim dsData As New DataSet
        Dim intRet As Integer = 0
        Dim strWork As String = vbNullString
        Dim strDate As String()
        Dim lngOrderAno As Long = 0
        Dim strPatientAno As String = vbNullString      '患者登録番号退避用

        Try
            '///サーバ日付、時刻の取得
            strWork = fncGetServerDate(2, strMsg)
            strDate = strWork.Split(",")

            Call subOutLog("オーダ情報登録(subInsOrder)：Start", 0)
            Call subDispMsg(0, "オーダ情報登録(subInsOrder)：Start", strMsg)

            '///intProcが"1"の時はDeleteInsert(論理削除)
            If intProc = 1 Then
                intRet = fncGetOrderAno(objOrder.オーダデータ.オーダ番号, lngOrderAno, strMsg)
                strSQL.Clear()
                strSQL.AppendLine("UPDATE TRAN_ORDER ")
                strSQL.AppendLine("SET ")
                strSQL.AppendLine(" DEL_FLG = '1',")
                strSQL.AppendLine(" UPD_DATE = '" & strDate(0).Trim & "',")
                strSQL.AppendLine(" UPD_TIME = '" & strDate(1).Trim & "',")
                strSQL.AppendLine(" UPD_USER = 'PRIM_ORDER_IF.subInsOrder' ")
                strSQL.AppendLine("WHERE ")
                strSQL.AppendLine(" ORDER_ANO = " & lngOrderAno)
                intRet = fncExecuteNonQuery(objConnect_LocalDB, strSQL.ToString, strMsg)

            End If

            With objOrder.オーダデータ

                If .オーダステータス <> "4" Then
                    '---患者登録番号取得 ADD By Watanabe 2014.07.17
                    Call fncGetPatientAno(.患者番号, strPatientAno, strMsg)
                    strSQL.Clear()
                    strSQL.AppendLine("INSERT INTO TRAN_ORDER ")
                    strSQL.AppendLine("(")
                    strSQL.AppendLine(" MWM_STATE,")
                    strSQL.AppendLine(" SEND_STATE,")
                    strSQL.AppendLine(" PATIENT_ANO,")
                    strSQL.AppendLine(" PATIENT_ID,")
                    strSQL.AppendLine(" SHIJI_DATE,")
                    strSQL.AppendLine(" SNONYM,")
                    strSQL.AppendLine(" SHINRYO_KBN,")
                    strSQL.AppendLine(" KUGIRI_NO,")
                    strSQL.AppendLine(" SHINRYO_KBN_NAME,")
                    strSQL.AppendLine(" SHINRYO_KBN_RNAME,")
                    strSQL.AppendLine(" KINKYU_KBN,")
                    strSQL.AppendLine(" KINKYU_KBN_NAME,")
                    strSQL.AppendLine(" JIGO_NYURYOKU_FLG,")
                    strSQL.AppendLine(" JIGO_NYURYOKU_FLG_NAME,")
                    strSQL.AppendLine(" HOKEN_SHUBETSU,")
                    strSQL.AppendLine(" HOKEN_SHUBETSU_NAME,")
                    strSQL.AppendLine(" SHINKI_FLG,")
                    strSQL.AppendLine(" SHINKI_FLG_NAME,")
                    strSQL.AppendLine(" KENSA_SHUBETSU,")
                    strSQL.AppendLine(" KENSA_SHUBETSU_NAME,")
                    strSQL.AppendLine(" SHOHO_KBN,")
                    strSQL.AppendLine(" SHOHO_KBN_NAME,")
                    strSQL.AppendLine(" TEIRINJI_FLG,")
                    strSQL.AppendLine(" TEIRINJI_FLG_NAME,")
                    strSQL.AppendLine(" NICHI_KAISU,")
                    strSQL.AppendLine(" JISSHIYOTEI_KAISHI_DATETIME,")
                    strSQL.AppendLine(" JISSHIYOTEI_KAISHI_JIKAN_KBN,")
                    strSQL.AppendLine(" JISSHIYOTEI_KAISHI_JIKAN_KBN_NAME,")
                    strSQL.AppendLine(" JISSHIYOTEI_DATE_KANKAKU,")
                    strSQL.AppendLine(" JISSHIYOTEI_SHURYO_JIKAN_KBN,")
                    strSQL.AppendLine(" JISSHIYOTEI_SHURYO_JIKAN_KBN_NAME,")
                    strSQL.AppendLine(" ORDER_NO,")
                    strSQL.AppendLine(" CHUSHI_HAMBETSU_FLG,")
                    strSQL.AppendLine(" CHUSHI_DATETIME,")
                    strSQL.AppendLine(" CHUSHI_JIKAN_KBN,")
                    strSQL.AppendLine(" CHUSHI_JIKAN_KBN_NAME,")
                    strSQL.AppendLine(" BUMONKA_CODE,")
                    strSQL.AppendLine(" BUMONKA_NAME,")
                    strSQL.AppendLine(" FREE_COMMENT,")
                    strSQL.AppendLine(" ORDER_STATUS,")
                    strSQL.AppendLine(" HUJISSHI_JOHO_DATETIME,")
                    strSQL.AppendLine(" HUJISSHI_JOHO_SOSASHA_ID,")
                    strSQL.AppendLine(" HUJISSHI_JOHO_SOSASHA_NAME,")
                    strSQL.AppendLine(" UKETSUKE_DATETIME,")
                    strSQL.AppendLine(" UKETSUKE_SOSASHA_ID,")
                    strSQL.AppendLine(" UKETSUKE_SOSASHA_NAME,")
                    strSQL.AppendLine(" SAKUJO_DATETIME,")
                    strSQL.AppendLine(" SAKUJO_SOSASHA_ID,")
                    strSQL.AppendLine(" SAKUJO_SOSASHA_NAME,")
                    strSQL.AppendLine(" CHUSHI_SOSA_DATETIME,")
                    strSQL.AppendLine(" CHUSHI_SOSASHA_ID,")
                    strSQL.AppendLine(" CHUSHI_SOSASHA_NAME,")
                    strSQL.AppendLine(" CHUSHI_SHIJII_SOSASHA_ID,")
                    strSQL.AppendLine(" CHUSHI_SHIJII_SOSASHA_NAME,")
                    strSQL.AppendLine(" HOSOKU_NYURYOKU_DATETIME,")
                    strSQL.AppendLine(" HOSOKU_NYURYOKU_SOSASHA_ID,")
                    strSQL.AppendLine(" HOSOKU_NYURYOKU_SOSASHA_NAME,")
                    strSQL.AppendLine(" KENSA_KEKKA_UMU,")
                    strSQL.AppendLine(" KENSA_KEKKA_UMU_NAME,")
                    strSQL.AppendLine(" SHOKUJI_SESSHU_YOHI,")
                    strSQL.AppendLine(" SHOKUJI_SESSHU_YOHI_NAME,")
                    strSQL.AppendLine(" KOHI_JOHO_REMBAN1,")
                    strSQL.AppendLine(" KOHI_JOHO_REMBAN1_NAME,")
                    strSQL.AppendLine(" KOHI_JOHO_REMBAN2,")
                    strSQL.AppendLine(" KOHI_JOHO_REMBAN2_NAME,")
                    strSQL.AppendLine(" NYUGAI_KBN,")
                    strSQL.AppendLine(" NYUGAI_KBN_NAME,")
                    strSQL.AppendLine(" SHINRYOKA_CODE,")
                    strSQL.AppendLine(" SHINRYOKA_NAME,")
                    strSQL.AppendLine(" BUMON_CODE,")
                    strSQL.AppendLine(" BUMON_NAME,")
                    strSQL.AppendLine(" SHINSATSUSHITSU_NO,")
                    strSQL.AppendLine(" SHINSATSUSHITSU_NAME,")
                    strSQL.AppendLine(" SOSASHA_ID,")
                    strSQL.AppendLine(" SOSASHA_NAME,")
                    strSQL.AppendLine(" SHIJII_ID,")
                    strSQL.AppendLine(" SHIJII_NAME,")
                    strSQL.AppendLine(" NYUIN_DATE,")
                    strSQL.AppendLine(" TAIIN_DATE,")
                    strSQL.AppendLine(" BYOTO_CODE,")
                    strSQL.AppendLine(" BYOTO_NAME,")
                    strSQL.AppendLine(" BYOSHITSU_NO,")
                    strSQL.AppendLine(" BYOSHITSU_NAME,")
                    strSQL.AppendLine(" NYUIN_KA,")
                    strSQL.AppendLine(" NYUIN_KA_NAME,")
                    strSQL.AppendLine(" SAISHU_TENTO_DATE,")
                    strSQL.AppendLine(" SHUJII_ID,")
                    strSQL.AppendLine(" SHUJII_NAME,")
                    If intProc = 0 Then
                        strSQL.AppendLine(" INS_DATE,")
                        strSQL.AppendLine(" INS_TIME,")
                        strSQL.AppendLine(" INS_USER,")
                    Else
                        strSQL.AppendLine(" UPD_DATE,")
                        strSQL.AppendLine(" UPD_TIME,")
                        strSQL.AppendLine(" UPD_USER,")
                    End If
                    strSQL.AppendLine(" DEL_FLG")
                    strSQL.AppendLine(") ")
                    strSQL.AppendLine(" VALUES ")
                    strSQL.AppendLine("(")
                    strSQL.AppendLine("'0',")
                    strSQL.AppendLine("'0',")
                    strSQL.AppendLine("" & CLng(strPatientAno) & ",")
                    strSQL.AppendLine("'" & .患者番号 & "',")
                    strSQL.AppendLine("'" & .指示日 & "',")
                    strSQL.AppendLine("'" & .シノニム.Trim & "',")
                    strSQL.AppendLine("'" & .診療区分 & "',")
                    strSQL.AppendLine("'" & .区切番号 & "',")
                    strSQL.AppendLine("'" & .診療区分名称 & "',")
                    strSQL.AppendLine("'" & .診療区分略称 & "',")
                    strSQL.AppendLine("'" & .緊急区分 & "',")
                    strSQL.AppendLine("'" & .緊急区分名称 & "',")
                    strSQL.AppendLine("'" & .事後入力フラグ & "',")
                    strSQL.AppendLine("'" & .事後入力フラグ名称 & "',")
                    strSQL.AppendLine("'" & .保険種別 & "',")
                    strSQL.AppendLine("'" & .保険種別名称 & "',")
                    strSQL.AppendLine("'" & .新規フラグ & "',")
                    strSQL.AppendLine("'" & .新規フラグ名称 & "',")
                    strSQL.AppendLine("'" & .検査種別 & "',")
                    strSQL.AppendLine("'" & .検査種別名称 & "',")
                    strSQL.AppendLine("'" & .処方区分 & "',")
                    strSQL.AppendLine("'" & .処方区分名称 & "',")
                    strSQL.AppendLine("'" & .定臨時フラグ & "',")
                    strSQL.AppendLine("'" & .定臨時フラグ名称 & "',")
                    strSQL.AppendLine("'" & .日回数 & "',")
                    strSQL.AppendLine("'" & .実施予定開始日時 & "',")
                    strSQL.AppendLine("'" & .実施予定開始時間区分 & "',")
                    strSQL.AppendLine("'" & .実施予定開始時間区分名称 & "',")
                    strSQL.AppendLine("'" & .実施予定日間隔 & "',")
                    strSQL.AppendLine("'" & .実施予定終了時間区分 & "',")
                    strSQL.AppendLine("'" & .実施予定終了時間区分名称 & "',")
                    strSQL.AppendLine("'" & .オーダ番号 & "',")
                    strSQL.AppendLine("'" & .中止判別フラグ & "',")
                    strSQL.AppendLine("'" & .中止日時 & "',")
                    strSQL.AppendLine("'" & .中止時間区分 & "',")
                    strSQL.AppendLine("'" & .中止時間区分名称 & "',")
                    strSQL.AppendLine("'" & .部門科コード & "',")
                    strSQL.AppendLine("'" & .部門科名称 & "',")
                    strSQL.AppendLine("'" & .フリーコメント & "',")
                    strSQL.AppendLine("'" & .オーダステータス & "',")
                    strSQL.AppendLine("'" & .不実施情報日時 & "',")
                    strSQL.AppendLine("'" & .不実施情報操作者.職員情報.職員ID & "',")
                    strSQL.AppendLine("'" & .不実施情報操作者.職員情報.職員氏名 & "',")
                    strSQL.AppendLine("'" & .受付日時 & "',")
                    strSQL.AppendLine("'" & .受付操作者.職員情報.職員ID & "',")
                    strSQL.AppendLine("'" & .受付操作者.職員情報.職員氏名 & "',")
                    strSQL.AppendLine("'" & .削除日時 & "',")
                    strSQL.AppendLine("'" & .削除操作者.職員情報.職員ID & "',")
                    strSQL.AppendLine("'" & .削除操作者.職員情報.職員氏名 & "',")
                    strSQL.AppendLine("'" & .中止操作日時 & "',")
                    strSQL.AppendLine("'" & .中止操作者.職員情報.職員ID & "',")
                    strSQL.AppendLine("'" & .中止操作者.職員情報.職員氏名 & "',")
                    strSQL.AppendLine("'" & .中止指示医.職員情報.職員ID & "',")
                    strSQL.AppendLine("'" & .中止指示医.職員情報.職員氏名 & "',")
                    strSQL.AppendLine("'" & .補足入力日時 & "',")
                    strSQL.AppendLine("'" & .補足入力操作者.職員情報.職員ID & "',")
                    strSQL.AppendLine("'" & .補足入力操作者.職員情報.職員氏名 & "',")
                    strSQL.AppendLine("'" & .検査結果有無 & "',")
                    strSQL.AppendLine("'" & .検査結果有無名称 & "',")
                    strSQL.AppendLine("'" & .食事摂取要否 & "',")
                    strSQL.AppendLine("'" & .食事摂取要否名称 & "',")
                    strSQL.AppendLine("'" & .公費情報連番1 & "',")
                    strSQL.AppendLine("'" & .公費情報連番1名称 & "',")
                    strSQL.AppendLine("'" & .公費情報連番2 & "',")
                    strSQL.AppendLine("'" & .公費情報連番2名称 & "',")
                    strSQL.AppendLine("'" & .管理情報.入外区分 & "',")
                    strSQL.AppendLine("'" & .管理情報.入外区分名称 & "',")
                    strSQL.AppendLine("'" & .管理情報.診療科コード & "',")
                    strSQL.AppendLine("'" & .管理情報.診療科名称 & "',")
                    strSQL.AppendLine("'" & .管理情報.部門コード & "',")
                    strSQL.AppendLine("'" & .管理情報.部門名称 & "',")
                    strSQL.AppendLine("'" & .管理情報.診察室番号 & "',")
                    strSQL.AppendLine("'" & .管理情報.診察室名称 & "',")
                    strSQL.AppendLine("'" & .管理情報.操作者.職員情報.職員ID & "',")
                    strSQL.AppendLine("'" & .管理情報.操作者.職員情報.職員氏名 & "',")
                    strSQL.AppendLine("'" & .管理情報.指示医.職員情報.職員ID & "',")
                    strSQL.AppendLine("'" & .管理情報.指示医.職員情報.職員氏名 & "',")
                    strSQL.AppendLine("'" & objOrder.入退院情報.入院日 & "',")
                    strSQL.AppendLine("'" & objOrder.入退院情報.退院日 & "',")
                    strSQL.AppendLine("'" & objOrder.入退院情報.病棟コード & "',")
                    strSQL.AppendLine("'" & objOrder.入退院情報.病棟名称 & "',")
                    strSQL.AppendLine("'" & objOrder.入退院情報.病室番号 & "',")
                    strSQL.AppendLine("'" & objOrder.入退院情報.病室名称 & "',")
                    strSQL.AppendLine("'" & objOrder.入退院情報.入院科 & "',")
                    strSQL.AppendLine("'" & objOrder.入退院情報.入院科名称 & "',")
                    strSQL.AppendLine("'" & objOrder.入退院情報.最終転棟転室日 & "',")
                    strSQL.AppendLine("'" & objOrder.入退院情報.主治医.職員ID & "',")
                    strSQL.AppendLine("'" & objOrder.入退院情報.主治医.職員氏名 & "',")
                    If intProc = 0 Then
                        strSQL.AppendLine("'" & strDate(0).Trim & "',")
                        strSQL.AppendLine("'" & strDate(1).Trim & "',")
                        strSQL.AppendLine("'PRIM-ORDER-IF.subInsOrder',")
                    Else
                        strSQL.AppendLine("'" & strDate(0).Trim & "',")
                        strSQL.AppendLine("'" & strDate(1).Trim & "',")
                        strSQL.AppendLine("'PRIM-ORDER-IF.subInsOrder',")
                    End If
                    strSQL.AppendLine("'0'")
                    strSQL.AppendLine(")")
                    intRet = fncExecuteNonQuery(objConnect_LocalDB, strSQL.ToString, strMsg)
                    Call subOutLog("オーダ情報登録SQL：" & strSQL.ToString, 0)
                    Call subOutLog("オーダ情報登録(subInsOrder)：正常終了", 0)
                    Call subDispMsg(0, "オーダ情報登録(subInsOrder)：正常終了", strMsg)
                Else
                    Call subOutLog("オーダ情報登録(subInsOrder)：中止オーダの為スキップ", 0)
                    Call subDispMsg(0, "オーダ情報登録(subInsOrder)：中止オーダの為スキップ", strMsg)
                End If
            End With
        Catch ex As Exception
            Call subOutLog("オーダ情報登録(subInsOrder) エラー " & strMsg, 1)
            Call subDispMsg(1, "オーダ情報登録(subInsOrder) エラー：" & strMsg, strMsg)
        Finally
            '///終了処理
            strSQL.Clear()
            strSQL = Nothing
        End Try
    End Sub
#End Region

#Region "オーダ明細情報登録(subInsOrderMeisai)"
    '**********************************************************************************
    '* 機能概要　：オーダ明細情報登録
    '* 関数名称　：subInsOrderMeisai
    '* 引　数　　：strMsg    =エラー内容退避用
    '* 作成日付　：2014.02.05
    '* 作成者　　：Created By Watanabe
    '* 更新履歴　：
    '**********************************************************************************
    Private Sub subInsOrderMeisai(ByRef strMsg As String)
        Dim lngCounter As Long = 0
        Dim lngFileCount As Long = 0
        Dim intRet As Integer = 0
        Dim lngOrderAno As Long = 0
        Dim strSQL As New System.Text.StringBuilder
        Dim strMWM_ORDER_NO As String = vbNullString
        Dim bolPortable As Boolean = False          '放射線科 ポータブル対応 ポータブルフラグ ADD By Watanabe 2022.02.18
        Dim strSiteHealth As String = vbNullString   '放射線科 施設健診判断Key退避 ADD By Watanabe 2022.04.21

        '放射線科 施設健診判断Key退避 ADD By Watanabe 2022.04.21
        strSiteHealth = My.Settings.SITEHEALTH_KEY

        Try
            Call subOutLog("オーダ明細情報登録(subInsOrderMeisai) Start", 0)
            '///オーダ登録番号取得
            intRet = fncGetOrderAno(objOrder.オーダデータ.オーダ番号, lngOrderAno, strMsg)
            '///オーダ番号生成
            strMWM_ORDER_NO = Date.Now.ToString("yyMMdd") & lngOrderAno.ToString.PadLeft(10).Replace(" ", "0")

            '///オーダ情報テーブルにオーダ番号を適用
            strSQL.Clear()
            strSQL.AppendLine("UPDATE TRAN_ORDER ")
            strSQL.AppendLine("SET MWM_ORDER_NO = '" & strMWM_ORDER_NO & "' ")
            strSQL.AppendLine("WHERE ")
            strSQL.AppendLine("ORDER_ANO = " & lngOrderAno & " ")
            intRet = fncExecuteNonQuery(objConnect_LocalDB, strSQL.ToString)

            '///明細数分繰り返す
            For lngCounter = 0 To objOrder.オーダデータ.明細.Length - 1
                With objOrder.オーダデータ.明細(lngCounter)
                    '///オーダ明細登録用SQL文生成
                    strSQL.Clear()
                    strSQL.AppendLine("INSERT INTO TRAN_ORDER_MEISAI ")
                    strSQL.AppendLine("(")
                    strSQL.AppendLine("ORDER_ANO,")
                    strSQL.AppendLine("MEISAI_NO,")
                    strSQL.AppendLine("NYURYOKU_KBN,")
                    strSQL.AppendLine("NYURYOKU_KBN_NAME,")
                    strSQL.AppendLine("KOUMKU_HIMBAN,")
                    strSQL.AppendLine("KOUMKU_NAME,")
                    strSQL.AppendLine("SURYO,")
                    strSQL.AppendLine("TANI,")
                    strSQL.AppendLine("SHIJI_COMMENT ")
                    strSQL.AppendLine(") ")
                    strSQL.AppendLine("VALUES ")
                    strSQL.AppendLine("(")
                    strSQL.AppendLine(lngOrderAno & ",")
                    strSQL.AppendLine("" & .明細番号 & ",")
                    strSQL.AppendLine("'" & .入力区分 & "',")
                    strSQL.AppendLine("'" & .入力区分名称 & "',")
                    strSQL.AppendLine("" & .項目品番 & ",")
                    strSQL.AppendLine("'" & .項目名称.Trim & "',")
                    strSQL.AppendLine("" & .数量 & ",")
                    strSQL.AppendLine("'" & .単位 & "',")
                    strSQL.AppendLine("'" & .指示コメント & "'")
                    strSQL.AppendLine(")")
                    '///SQL発行
                    '---SQLログ出力をエラー時のみに変更 Deleted By Watanabe 2014.08.06
                    'Call subOutLog("オーダ明細情報登録(subInsOrderMeisai) SQL:" & strSQL.ToString, 0)
                    intRet = fncExecuteNonQuery(objConnect_LocalDB, strSQL.ToString, strMsg)

                    '---START 放射線科 施設健診対応 部位＆指示コメント ADD By Watanabe 2022.02.18
                    '---START 放射線科 ポータブル対応 部位＆指示コメント ADD By Watanabe 2022.02.18
                    '---項目名称に「ポータ」という文字列が含まれていればPortableフラグをTrueにする
                    If InStr(.項目名称, strSiteHealth) > 0 Then bolPortable = True
                    '---END 放射線科 ポータブル対応 部位＆指示コメント ADD By Watanabe 2022.02.18

                End With
            Next lngCounter

            '---START 放射線科 施設健診 部位＆指示コメント ADD By Watanabe 2022.04.22
            '---START 放射線科 ポータブル対応 部位＆指示コメント ADD By Watanabe 2022.02.18
            '---ポータブルフラグがTrueならTRN_ORDER.PORTABLEに"1"をセットする。
            If bolPortable = True Then
                strSQL.Clear()
                strSQL.AppendLine("UPDATE TRN_ORDER")
                strSQL.AppendLine("SET SITEHEALTH = '1'")
                strSQL.AppendLine("WHERE ORDER_ANO = " & lngOrderAno)

                intRet = fncExecuteNonQuery(objConnect_LocalDB, strSQL.ToString, strMsg)
            End If
            '---END 放射線科 ポータブル対応 部位＆指示コメント ADD By Watanabe 2022.02.18
            '---END 放射線科 施設健診 部位＆指示コメント ADD By Watanabe 2022.04.22

        Catch ex As Exception
            strMsg = ex.Message
            Call subOutLog("オーダ明細情報登録(subInsOrderMeisai) エラー：" & strMsg, 1)
            '---SQLログ出力をエラー時のみに変更 Deleted By Watanabe 2014.08.06
            If strSQL.ToString <> "" Then
                Call subOutLog("オーダ明細情報登録(subInsOrderMeisai) SQL:" & strSQL.ToString, 1)
            End If
        End Try
    End Sub
#End Region

#Region "オーダ詳細情報登録(subInsOrderShosai)"
    '**********************************************************************************
    '* 機能概要　：オーダ詳細情報登録
    '* 関数名称　：subInsOrderShosai
    '* 引　数　　：strMsg    =エラー内容退避用
    '* 作成日付　：2014.02.05
    '* 作成者　　：Created By Watanabe
    '* 更新履歴　：
    '**********************************************************************************
    Private Sub subInsOrderShosai(ByRef strMsg As String)
        Dim lngCounter As Long = 0
        Dim lngFileCount As Long = 0
        Dim intRet As Integer = 0
        Dim lngOrderAno As Long = 0
        Dim strSQL As New System.Text.StringBuilder
        Dim lngCounter2 As Long = 0


        Try
            Call subOutLog("オーダ詳細情報登録(subInsOrderShosai) Start", 0)
            '///オーダ登録番号取得
            intRet = fncGetOrderAno(objOrder.オーダデータ.オーダ番号, lngOrderAno, strMsg)

            If objOrder.オーダデータ.詳細.フレーム Is Nothing Then Exit Sub

            '///明細数分繰り返す
            For lngCounter = 0 To objOrder.オーダデータ.詳細.フレーム.Length - 1
                With objOrder.オーダデータ.詳細.フレーム(lngCounter)
                    '///オーダ明細登録用SQL文生成
                    strSQL.Clear()
                    strSQL.AppendLine("INSERT INTO TRAN_ORDER_FRAME ")
                    strSQL.AppendLine("(")
                    strSQL.AppendLine("ORDER_ANO,")
                    strSQL.AppendLine("FRAME_NO,")
                    strSQL.AppendLine("FRAME_NAME ")
                    strSQL.AppendLine(") ")
                    strSQL.AppendLine("VALUES ")
                    strSQL.AppendLine("(")
                    strSQL.AppendLine(lngOrderAno & ",")
                    strSQL.AppendLine("" & .フレーム番号 & ",")
                    strSQL.AppendLine("'" & .フレーム名称.Trim & "' ")
                    strSQL.AppendLine(")")
                    '---SQLログ出力をエラー時のみに変更 Deleted By Watanabe 2014.08.06
                    'Call subOutLog("オーダ詳細情報登録(subInsOrderShosai) フレーム部 SQL:" & strSQL.ToString, 0)
                    '///SQL発行
                    intRet = fncExecuteNonQuery(objConnect_LocalDB, strSQL.ToString, strMsg)

                End With

                Dim lngFrameAno As Long = 0
                intRet = fncGetFrameAno(lngOrderAno, lngFrameAno, strMsg)

                '///詳細項目数分繰り返す
                For lngCounter2 = 0 To objOrder.オーダデータ.詳細.フレーム(lngCounter).詳細項目.Length - 1

                    With objOrder.オーダデータ.詳細.フレーム(lngCounter).詳細項目(lngCounter2)
                        '///オーダ明細登録用SQL文生成
                        strSQL.Clear()
                        strSQL.AppendLine("INSERT INTO TRAN_ORDER_FRAME_SHOSAI ")
                        strSQL.AppendLine("(")
                        strSQL.AppendLine("ORDER_ANO,")
                        strSQL.AppendLine("FRAME_ANO,")
                        strSQL.AppendLine("ITEM_NO,")
                        strSQL.AppendLine("ITEM_NAME,")
                        strSQL.AppendLine("ITEM_VALUE ")
                        strSQL.AppendLine(") ")
                        strSQL.AppendLine("VALUES ")
                        strSQL.AppendLine("(")
                        strSQL.AppendLine(lngOrderAno & ",")
                        strSQL.AppendLine(lngFrameAno & ",")
                        strSQL.AppendLine("" & .項目番号 & ",")
                        strSQL.AppendLine("'" & .項目名称 & "',")
                        strSQL.AppendLine("'" & .登録内容 & "' ")
                        strSQL.AppendLine(")")
                        '///SQL発行
                        '---SQLログ出力をエラー時のみに変更 Deleted By Watanabe 2014.08.06
                        'Call subOutLog("オーダ詳細情報登録(subInsOrderShosai) SQL:" & strSQL.ToString, 0)
                        intRet = fncExecuteNonQuery(objConnect_LocalDB, strSQL.ToString, strMsg)

                    End With
                Next lngCounter2
            Next lngCounter



        Catch ex As Exception
            strMsg = ex.Message
            Call subOutLog("オーダ明細情報登録(subInsOrderMeisai) エラー：" & strMsg, 1)
            '---SQLログ出力をエラー時のみに変更 ADD By Watanabe 2014.08.06
            If strSQL.ToString <> "" Then
                Call subOutLog("オーダ詳細情報登録(subInsOrderShosai) SQL:" & strSQL.ToString, 1)
            End If
        End Try
    End Sub
#End Region

#Region "オーダ登録番号取得(fncGetOrderAno)"
    '**********************************************************************************
    '* 機能概要　：オーダ登録番号取得
    '* 関数名称　：fncGetOrderAno
    '* 引　数　　：strOrderNO    =オーダ番号(検索キー)
    '* 　　　　　：lngOrderAno　 =取得オーダ登録番号退避
    '* 　　　　　：strMsg　　　　=エラーメッセージ退避
    '* 戻り値　　：Integer　　　 =RET_NORMAL(0)      ・・・正常終了
    '* 　　　　　：       　　　 =RET_NOTFOUND(-2)   ・データ無
    '* 　　　　　：       　　　 =RET_ERROR(-9)      ・・・異常終了
    '* 作成日付　：2014.02.05
    '* 作成者　　：Created By Watanabe
    '* 更新履歴　：
    '**********************************************************************************
    Private Function fncGetOrderAno(ByVal strOrderNO As String, ByRef lngOrderAno As Long, ByRef strMsg As String) As Integer
        Dim strSQL As New System.Text.StringBuilder
        Dim dsData As New DataSet

        Try
            '///SQL文生成
            strSQL.Clear()
            strSQL.AppendLine("SELECT MAX(ORDER_ANO) AS MAX_ANO ")
            strSQL.AppendLine("FROM TRAN_ORDER ")
            strSQL.AppendLine("WHERE ")
            '---条件を変更(fncChkOrderと同様に) ADD By Watanabe 2014.08.06
            'strSQL.AppendLine("ORDER_NO = '" & strOrderNO & "' ")
            'strSQL.AppendLine("AND ")
            'strSQL.AppendLine("DEL_FLG = '0' ")
            strSQL.AppendLine("CONVERT(varchar,SHIJI_DATE,112) = '" & objOrder.オーダデータ.指示日.Replace("/", "") & "' ")
            strSQL.AppendLine("AND ")
            strSQL.AppendLine("PATIENT_ID = '" & objOrder.オーダデータ.患者番号 & "' ")
            strSQL.AppendLine("AND ")
            strSQL.AppendLine("CONVERT(varchar,JISSHIYOTEI_KAISHI_DATETIME,112) = '" & objOrder.オーダデータ.実施予定開始日時.Substring(0, 10).Replace("/", "") & "'")
            strSQL.AppendLine("AND ")
            strSQL.AppendLine("SNONYM = '" & objOrder.オーダデータ.シノニム & "' ")
            strSQL.AppendLine("AND ")
            strSQL.AppendLine("KUGIRI_NO = '" & objOrder.オーダデータ.区切番号 & "' ")
            strSQL.AppendLine("AND ")
            strSQL.AppendLine("SHINRYO_KBN = '" & objOrder.オーダデータ.診療区分 & "' ")
            strSQL.AppendLine("AND ")
            strSQL.AppendLine("DEL_FLG = '0' ")

            '///SQL発行
            dsData = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)
            '///データ無
            If dsData.Tables.Count = 0 Then Return RET_NOTFOUND
            If dsData.Tables(0).Rows.Count = 0 Then Return RET_NOTFOUND
            '///取得データを引数にセット
            lngOrderAno = dsData.Tables(0).Rows(0).Item("MAX_ANO")

            Return RET_NORMAL
        Catch ex As Exception
            strMsg = ex.Message
            Call subOutLog("オーダ登録番号取得(fncGetOrderAno) エラー：" & strMsg, 1)
            Return RET_ERROR

        End Try
    End Function
#End Region

#Region "フレーム登録番号取得(fncGetFrameAno)"
    '**********************************************************************************
    '* 機能概要　：フレーム登録番号取得
    '* 関数名称　：fncGetFrameAno
    '* 引　数　　：lngOrderAno　 =取得オーダ登録番号退避
    '* 　　　　　：strMsg　　　　=エラーメッセージ退避
    '* 戻り値　　：Integer　　　 =RET_NORMAL(0)      ・・・正常終了
    '* 　　　　　：       　　　 =RET_NOTFOUND(-2)   ・データ無
    '* 　　　　　：       　　　 =RET_ERROR(-9)      ・・・異常終了
    '* 作成日付　：2014.02.05
    '* 作成者　　：Created By Watanabe
    '* 更新履歴　：
    '**********************************************************************************
    Private Function fncGetFrameAno(ByVal lngOrderAno As String, ByRef lngFrameAno As Long, ByRef strMsg As String) As Integer
        Dim strSQL As New System.Text.StringBuilder
        Dim dsData As New DataSet

        Try
            '///SQL文生成
            strSQL.Clear()
            strSQL.AppendLine("SELECT MAX(FRAME_ANO) AS MAX_ANO ")
            strSQL.AppendLine("FROM TRAN_ORDER_FRAME ")
            strSQL.AppendLine("WHERE ")
            strSQL.AppendLine("ORDER_ANO = '" & lngOrderAno & "' ")
            '///SQL発行
            dsData = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)
            '///データ無
            If dsData.Tables.Count = 0 Then Return RET_NOTFOUND
            If dsData.Tables(0).Rows.Count = 0 Then Return RET_NOTFOUND
            '///取得データを引数にセット
            lngFrameAno = dsData.Tables(0).Rows(0).Item("MAX_ANO")

            Return RET_NORMAL
        Catch ex As Exception
            strMsg = ex.Message
            Call subOutLog("フレーム登録番号取得(fncGetFrameAno) エラー：" & strMsg, 1)
            Return RET_ERROR

        End Try
    End Function
#End Region

#Region "オーダテーブル患者登録番号更新(subUpdOrderPatientAno)"
    '**********************************************************************************
    '* 機能概要　：オーダテーブル患者登録番号更新
    '* 関数名称　：subUpdOrderPatientAno
    '* 引　数　　：strPatientAno  =取得患者番号
    '* 　　　　　：lngOrderAno    =取得オーダ登録番号退避
    '* 　　　　　：strMsg         =エラーメッセージ退避
    '* 戻り値　　：無
    '* 作成日付　：2014.02.05
    '* 作成者　　：Created By Watanabe
    '* 更新履歴　：
    '**********************************************************************************
    Private Sub subUpdOrderPatientAno(ByVal strPatientAno As String, ByVal strPatientID As String, ByVal strOrderAno As String, ByRef strMsg As String)
        Dim strSQL As New System.Text.StringBuilder
        Dim intRet As Integer = 0

        Try
            '///SQL文生成
            strSQL.Clear()
            strSQL.AppendLine("UPDATE TRAN_ORDER ")
            strSQL.AppendLine("SET ")
            strSQL.AppendLine("PATIENT_ANO = " & strPatientAno & ",")
            strSQL.AppendLine("SEND_STATE = '0' ")
            strSQL.AppendLine("WHERE ")
            strSQL.AppendLine(" (MWM_STATE = '0' or MWM_STATE = '1') ")
            strSQL.AppendLine("AND ")
            strSQL.AppendLine(" PATIENT_ID = '" & strPatientID & "' ")
            strSQL.AppendLine("AND ")
            strSQL.AppendLine("DEL_FLG = '0' ")
            'strSQL.AppendLine("ORDER_ANO = " & strOrderAno)
            '///SQL発行
            intRet = fncExecuteNonQuery(objConnect_LocalDB, strSQL.ToString, strMsg)

        Catch ex As Exception
            strMsg = ex.Message
            Call subOutLog("オーダテーブル患者登録番号更新(subUpdOrderPatientAno) エラー：" & strMsg, 1)
        Finally
            strSQL.Clear()
            strSQL = Nothing

        End Try
    End Sub
#End Region

#End Region

#Region "患者情報系"

#Region "患者情報存在チェック(fncChkPatient)"
    '**********************************************************************************
    '* 機能概要　：患者情報存在チェック
    '* 関数名称　：fncChkOrder
    '* 引　数　　：strMsg    =エラー内容退避用
    '* 戻り値　　：Integer・・・RET_NORMAL(0)=正常、RET_NOTFOUND(-1)、
    '* 　　　　　：       　　　RET_WARNING(-2)=警告、RET_ERROR(-9)=異常
    '* 作成日付　：2014.02.05
    '* 作成者　　：Created By Watanabe
    '* 更新履歴　：
    '**********************************************************************************
    Private Function fncChkPatient(ByVal strPatientNo As String, ByRef strPatientAno As String, ByRef strMsg As String) As Integer
        Dim strSQL As New System.Text.StringBuilder     'SQL文編集用
        Dim dsData As New DataSet                       '取得データ退避用

        Try
            Call subOutLog("患者情報存在チェック(fncChkPatient)：Start", 0)
            Call subDispMsg(0, "患者情報存在チェック(fncChkPatient)：Start", strMsg)

            '///SQL文編集
            strSQL.Clear()
            strSQL.AppendLine("SELECT ISNULL(MAX(PATIENT_ANO),0) AS MAX_PATIENT_ANO ")
            strSQL.AppendLine("FROM TRAN_PATIENT ")
            strSQL.AppendLine("WHERE KANJA_NO = '" & strPatientNo & "'")
            '///SQL発行
            dsData = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)

            Call subOutLog("患者情報存在チェック(fncChkPatient) SQL：" & strSQL.ToString, 0)

            '///存在データ無
            If dsData.Tables.Count = 0 Then Return RET_NOTFOUND
            If dsData.Tables(0).Rows.Count = 0 Then Return RET_NOTFOUND
            If dsData.Tables(0).Rows(0).Item("MAX_PATIENT_ANO") = 0 Then Return RET_NOTFOUND

            strPatientAno = dsData.Tables(0).Rows(0).Item("MAX_PATIENT_ANO")

            Call subOutLog("患者情報存在チェック(fncChkPatient)：正常終了", 0)
            Call subDispMsg(0, "患者情報存在チェック(fncChkPatient)：正常終了", strMsg)

            Return RET_NORMAL

        Catch ex As Exception
            strMsg = ex.Message
            Call subOutLog("患者情報存在チェック(fncChkPatient) エラー：" & strMsg, 1)
            Call subDispMsg(1, "患者情報存在チェック(fncChkPatient) エラー：", strMsg)

            Return RET_ERROR

        Finally
            '///終了処理
            strSQL.Clear()
            strSQL = Nothing
            dsData.Dispose()
            dsData = Nothing
        End Try
    End Function
#End Region

#Region "患者情報登録(subInsPatient)"
    '**********************************************************************************
    '* 機能概要　：患者情報登録処理
    '* 関数名称　：subInsPatient
    '* 引　数　　：strMsg    =エラー内容退避用
    '* 作成日付　：2014.02.05
    '* 作成者　　：Created By Watanabe
    '* 更新履歴　：
    '**********************************************************************************
    Private Sub subInsPatient(ByRef intProc As Integer, ByRef strMsg As String)
        Dim strSQL As New System.Text.StringBuilder
        Dim dsData As New DataSet
        Dim intRet As Integer = 0
        Dim strWork As String = vbNullString
        Dim strDate As String()
        Dim lngOrderAno As Long = 0
        Dim objK2R As New clsKana2Ro
        Dim lngPatientAno As Long = 0

        Try
            '///サーバ日付、時刻の取得
            strWork = fncGetServerDate(2, strMsg)
            strDate = strWork.Split(",")

            Call subOutLog("患者情報登録処理(subInsPatient)：Start", 0)
            Call subDispMsg(0, "患者情報登録処理(subInsPatient)：Start", strMsg)

            '///intProcが"1"の時はDeleteInsert(論理削除)
            If intProc = 1 Then
                intRet = fncGetPatientAno(objOrder.オーダデータ.患者番号, lngPatientAno, strMsg)

                strSQL.Clear()
                strSQL.AppendLine("SELECT ")
                strSQL.AppendLine(" KANJI_NAME,")
                strSQL.AppendLine(" KANA_NAME,")
                strSQL.AppendLine(" SEIBETSU,")
                strSQL.AppendLine(" SEIBETSU_NAME,")
                strSQL.AppendLine(" SHINCHO,")
                strSQL.AppendLine(" TAIJYU,")
                strSQL.AppendLine(" BIRTH_DATE,")
                strSQL.AppendLine(" YUBIN_NO,")
                strSQL.AppendLine(" ADDRESS,")
                strSQL.AppendLine(" BLOOD_TYPE_ABO,")
                strSQL.AppendLine(" BLOOD_TYPE_RH,")
                strSQL.AppendLine(" BLOOD_TYPE_DATE ")
                strSQL.AppendLine("FROM ")
                strSQL.AppendLine(" TRAN_PATIENT ")
                strSQL.AppendLine("WHERE ")
                strSQL.AppendLine(" PATIENT_ANO = " & lngPatientAno & " ")
                dsData = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)
                '///元データとの変更点確認
                With objOrder.患者情報
                    If .カナ氏名.Trim = dsData.Tables(0).Rows(0).Item("KANA_NAME") And .漢字氏名 = dsData.Tables(0).Rows(0).Item("KANJI_NAME") And _
                        .性別 = dsData.Tables(0).Rows(0).Item("SEIBETSU") And .生年月日 = dsData.Tables(0).Rows(0).Item("BIRTH_DATE") And _
                        .身長 = dsData.Tables(0).Rows(0).Item("SHINCHO") And .体重 = dsData.Tables(0).Rows(0).Item("TAIJYU") And _
                        .郵便番号 = dsData.Tables(0).Rows(0).Item("YUBIN_NO") And .住所 = dsData.Tables(0).Rows(0).Item("ADDRESS") And _
                        .身長 = dsData.Tables(0).Rows(0).Item("SHINCHO") And .体重 = dsData.Tables(0).Rows(0).Item("TAIJYU") Then
                    Else
                        intProc = 0
                        strSQL.Clear()
                        strSQL.AppendLine("UPDATE TRAN_PATIENT ")
                        strSQL.AppendLine("SET ")
                        strSQL.AppendLine(" DEL_FLG = '1',")
                        strSQL.AppendLine(" UPD_DATE = '" & strDate(0).Trim & "',")
                        strSQL.AppendLine(" UPD_TIME = '" & strDate(1).Trim & "',")
                        strSQL.AppendLine("UPD_USER = 'PRIM_ORDER_IF.subInsPatient' ")
                        strSQL.AppendLine("WHERE ")
                        strSQL.AppendLine(" PATIENT_ANO = " & lngPatientAno)
                        intRet = fncExecuteNonQuery(objConnect_LocalDB, strSQL.ToString, strMsg)
                    End If
                End With


            End If
            '///患者情報登録用SQL文生成
            With objOrder.患者情報

                If intProc = 0 Then
                    strSQL.Clear()
                    strSQL.AppendLine("INSERT INTO TRAN_PATIENT ")
                    strSQL.AppendLine("(")
                    strSQL.AppendLine(" KANJA_NO,")
                    strSQL.AppendLine(" KANJI_NAME,")
                    strSQL.AppendLine(" KANA_NAME,")
                    strSQL.AppendLine(" EIJI_NAME,")
                    strSQL.AppendLine(" SEIBETSU,")
                    strSQL.AppendLine(" SEIBETSU_NAME,")
                    strSQL.AppendLine(" ADDRESS,")
                    strSQL.AppendLine(" SHINCHO,")
                    strSQL.AppendLine(" TAIJYU,")
                    strSQL.AppendLine(" BIRTH_DATE,")
                    strSQL.AppendLine(" YUBIN_NO,")
                    strSQL.AppendLine(" TEL_NO,")
                    strSQL.AppendLine(" RENRAKUSAKI_TEL_NO,")
                    strSQL.AppendLine(" OLD_KANA_NAME,")
                    strSQL.AppendLine(" OLD_KANJI_NAME,")
                    strSQL.AppendLine(" OLD_NAME_YUKOKIGEN,")
                    strSQL.AppendLine(" BLOOD_TYPE_ABO,")
                    strSQL.AppendLine(" BLOOD_TYPE_RH,")
                    strSQL.AppendLine(" BLOOD_TYPE_DATE,")
                    strSQL.AppendLine(" NINSHIN_UMU,")
                    strSQL.AppendLine(" BUNBEN_YOTEI_DATE,")
                    strSQL.AppendLine(" NINSHIN_SHUSU,")
                    strSQL.AppendLine(" HOKOU,")
                    strSQL.AppendLine(" HOKOU_NAME,")
                    strSQL.AppendLine(" ANSEIDO,")
                    strSQL.AppendLine(" ANSEIDO_NAME,")
                    strSQL.AppendLine(" KANGODO,")
                    strSQL.AppendLine(" SHOKUJI_KAIJODO,")
                    strSQL.AppendLine(" SHOKUJI_KAIJODO_NAME,")
                    strSQL.AppendLine(" HUKUYAKU_SHIDO,")
                    strSQL.AppendLine(" HUKUYAKU_SHIDO_NAME,")
                    strSQL.AppendLine(" NINI_JOHO,")
                    strSQL.AppendLine(" NINI_JOHO_NAME,")
                    If intProc = 0 Then
                        strSQL.AppendLine("INS_DATE,")
                        strSQL.AppendLine("INS_TIME,")
                        strSQL.AppendLine("INS_USER,")
                    Else
                        strSQL.AppendLine("UPD_DATE,")
                        strSQL.AppendLine("UPD_TIME,")
                        strSQL.AppendLine("UPD_USER,")
                    End If
                    strSQL.AppendLine("DEL_FLG")
                    strSQL.AppendLine(") ")
                    strSQL.AppendLine("VALUES ")
                    strSQL.AppendLine("(")
                    strSQL.AppendLine("'" & objOrder.オーダデータ.患者番号 & "',")
                    strSQL.AppendLine("'" & .漢字氏名.Trim & "',")
                    strSQL.AppendLine("'" & .カナ氏名.Trim & "',")
                    strSQL.AppendLine("'" & StrConv(objK2R.kana2ro(.カナ氏名), VbStrConv.Uppercase) & "',")
                    strSQL.AppendLine("'" & .性別.Trim & "',")
                    strSQL.AppendLine("'" & .性別名称.Trim & "',")
                    strSQL.AppendLine("'" & .住所.Trim & "',")
                    strSQL.AppendLine("'" & .身長.Trim & "',")
                    strSQL.AppendLine("'" & .体重.Trim & "',")
                    strSQL.AppendLine("'" & .生年月日.Trim & "',")
                    strSQL.AppendLine("'" & .郵便番号.Trim & "',")
                    strSQL.AppendLine("'" & .電話番号.Trim & "',")
                    strSQL.AppendLine("'" & .連絡先電話番号.Trim & "',")
                    strSQL.AppendLine("'" & .旧カナ氏名.Trim & "',")
                    strSQL.AppendLine("'" & .旧漢字氏名.Trim & "',")
                    strSQL.AppendLine("'" & .旧氏名有効期限.Trim & "',")
                    strSQL.AppendLine("'" & .血液型ABO.Trim & "',")
                    strSQL.AppendLine("'" & .血液型RH.Trim & "',")
                    strSQL.AppendLine("'" & .血液型最終確認日.Trim & "',")
                    strSQL.AppendLine("'" & .妊娠有無.Trim & "',")
                    '                strSQL.AppendLine("'" & .妊娠有無名称 & "',")
                    strSQL.AppendLine("'" & .分娩予定日.Trim & "',")
                    strSQL.AppendLine("'" & .妊娠週数.Trim & "',")
                    strSQL.AppendLine("'" & .歩行状態.Trim & "',")
                    strSQL.AppendLine("'" & .歩行状態名称 & "',")
                    strSQL.AppendLine("'" & .安静度.Trim & "',")
                    strSQL.AppendLine("'" & .安静度名称 & "',")
                    strSQL.AppendLine("'" & .看護度 & "',")
                    strSQL.AppendLine("'" & .食事介助度 & "',")
                    strSQL.AppendLine("'" & .食事介助度名称 & "',")
                    strSQL.AppendLine("'" & .服薬指導 & "',")
                    strSQL.AppendLine("'" & .服薬指導名称 & "',")
                    strSQL.AppendLine("'" & .任意情報 & "',")
                    strSQL.AppendLine("'" & .任意情報名称 & "',")
                    strSQL.AppendLine("'" & strDate(0).Trim & "',")
                    strSQL.AppendLine("'" & strDate(1).Trim & "',")
                    strSQL.AppendLine("'PRIM-ORDER-IF.subInsPatient',")
                    strSQL.AppendLine("'0'")
                    strSQL.AppendLine(")")
                    '///SQL発行
                    intRet = fncExecuteNonQuery(objConnect_LocalDB, strSQL.ToString, strMsg)
                    Call subOutLog("患者情報登録(subInsPatient)：正常終了", 0)
                    Call subDispMsg(0, "患者情報登録(subInsPatient)：正常終了", strMsg)
                Else
                    strSQL.Clear()
                    strSQL.AppendLine("UPDATE ")
                    strSQL.AppendLine("TRAN_PATIENT ")
                    strSQL.AppendLine("SET ")
                    strSQL.AppendLine(" KANJA_NO='" & .患者番号 & "',")
                    strSQL.AppendLine(" KANJI_NAME='" & .漢字氏名 & "',")
                    strSQL.AppendLine(" KANA_NAME='" & .カナ氏名 & "',")
                    strSQL.AppendLine(" EIJI_NAME='" & StrConv(objK2R.kana2ro(.カナ氏名), VbStrConv.Uppercase) & "',")
                    strSQL.AppendLine(" SEIBETSU='" & .性別 & "',")
                    strSQL.AppendLine(" SEIBETSU_NAME='" & .性別名称 & "',")
                    strSQL.AppendLine(" ADDRESS='" & .住所 & "',")
                    strSQL.AppendLine(" SHINCHO='" & .身長 & "',")
                    strSQL.AppendLine(" TAIJYU='" & .体重 & "',")
                    strSQL.AppendLine(" BIRTH_DATE='" & .生年月日 & "',")
                    strSQL.AppendLine(" YUBIN_NO='" & .郵便番号 & "',")
                    strSQL.AppendLine(" TEL_NO='" & .電話番号 & "',")
                    strSQL.AppendLine(" RENRAKUSAKI_TEL_NO='" & .連絡先電話番号 & "',")
                    strSQL.AppendLine(" OLD_KANA_NAME='" & .旧カナ氏名 & "',")
                    strSQL.AppendLine(" OLD_KANJI_NAME='" & .旧漢字氏名 & "',")
                    strSQL.AppendLine(" OLD_NAME_YUKOKIGEN='" & .旧氏名有効期限 & "',")
                    strSQL.AppendLine(" BLOOD_TYPE_ABO='" & .血液型ABO & "',")
                    strSQL.AppendLine(" BLOOD_TYPE_RH='" & .血液型RH & "',")
                    strSQL.AppendLine(" BLOOD_TYPE_DATE='" & .血液型最終確認日 & "',")
                    strSQL.AppendLine(" NINSHIN_UMU='" & .妊娠有無 & "',")
                    strSQL.AppendLine(" BUNBEN_YOTEI_DATE='" & .分娩予定日 & "',")
                    strSQL.AppendLine(" NINSHIN_SHUSU='" & .妊娠週数 & "',")
                    strSQL.AppendLine(" HOKOU='" & .歩行状態 & "',")
                    strSQL.AppendLine(" HOKOU_NAME='" & .歩行状態名称 & "',")
                    strSQL.AppendLine(" ANSEIDO='" & .安静度 & "',")
                    strSQL.AppendLine(" ANSEIDO_NAME='" & .安静度名称 & "',")
                    strSQL.AppendLine(" KANGODO='" & .看護度 & "',")
                    strSQL.AppendLine(" SHOKUJI_KAIJODO='" & .食事介助度 & "',")
                    strSQL.AppendLine(" SHOKUJI_KAIJODO_NAME='" & .食事介助度名称 & "',")
                    strSQL.AppendLine(" HUKUYAKU_SHIDO='" & .服薬指導 & "',")
                    strSQL.AppendLine(" HUKUYAKU_SHIDO_NAME='" & .服薬指導名称 & "',")
                    strSQL.AppendLine(" NINI_JOHO='" & .任意情報 & "',")
                    strSQL.AppendLine(" NINI_JOHO_NAME='" & .任意情報名称 & "',")
                    strSQL.AppendLine(" UPD_DATE='" & strDate(0).Trim & "',")
                    strSQL.AppendLine(" UPD_TIME='" & strDate(1).Trim & "',")
                    strSQL.AppendLine(" UPD_USER='PRIM-ORDER-IF.subInsPatient' ")
                    strSQL.AppendLine("WHERE ")
                    strSQL.AppendLine(" PATIENT_ANO = " & lngPatientAno)
                    '///SQL発行
                    intRet = fncExecuteNonQuery(objConnect_LocalDB, strSQL.ToString, strMsg)
                End If
            End With
        Catch ex As Exception
            Call subOutLog("患者情報登録(subInsPatient) エラー " & strMsg, 1)
            Call subDispMsg(1, "患者情報登録(subInsPatient) エラー：" & strMsg, strMsg)
        Finally
            '///終了処理
            strSQL.Clear()
            strSQL = Nothing
        End Try
    End Sub
#End Region

#Region "患者登録番号取得(fncGetPatientAno)"
    '**********************************************************************************
    '* 機能概要　：患者登録番号取得
    '* 関数名称　：fncGetPatientAno
    '* 引　数　　：strPatientID       =患者ID(検索キー)
    '* 　　　　　：lngPatientAno      =取得患者登録番号退避
    '* 　　　　　：strMsg             =エラーメッセージ退避
    '* 戻り値　　：Integer            =RET_NORMAL(0)      ・・・正常終了
    '* 　　　　　：                   =RET_NOTFOUND(-2)   ・・・データ無
    '* 　　　　　：                   =RET_ERROR(-9)      ・・・異常終了
    '* 作成日付　：2014.02.05
    '* 作成者　　：Created By Watanabe
    '* 更新履歴　：
    '**********************************************************************************
    Private Function fncGetPatientAno(ByVal strPatientID As String, ByRef lngPatientAno As Long, ByRef strMsg As String) As Integer
        Dim strSQL As New System.Text.StringBuilder
        Dim dsData As New DataSet

        Try
            '///SQL文生成
            strSQL.AppendLine("SELECT MAX(PATIENT_ANO) AS MAX_ANO ")
            strSQL.AppendLine("FROM TRAN_PATIENT ")
            strSQL.AppendLine("WHERE ")
            strSQL.AppendLine("KANJA_NO = '" & strPatientID & "' ")
            strSQL.AppendLine("AND ")
            strSQL.AppendLine("DEL_FLG = '0' ")
            dsData = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)
            '///データ無
            If dsData.Tables.Count = 0 Then Return RET_NOTFOUND
            If dsData.Tables(0).Rows.Count = 0 Then Return RET_NOTFOUND
            '///取得データを引数にセット
            lngPatientAno = dsData.Tables(0).Rows(0).Item("MAX_ANO")

            Return RET_NORMAL
        Catch ex As Exception
            strMsg = ex.Message
            Call subOutLog("患者登録番号取得(fncGetPatientAno) エラー：" & strMsg, 1)
            Return RET_ERROR

        End Try
    End Function
#End Region

#Region "患者常用薬情報登録(subInsPatientJoyoyaku)"
    '**********************************************************************************
    '* 機能概要　：患者常用薬情報登録
    '* 関数名称　：subInsPatientJoyoyaku
    '* 引　数　　：strMsg    =エラー内容退避用
    '* 作成日付　：2014.02.05
    '* 作成者　　：Created By Watanabe
    '* 更新履歴　：
    '**********************************************************************************
    Private Sub subInsPatientJoyoyaku(ByRef strMsg As String)
        Dim lngCounter As Long = 0
        Dim strSQL As New System.Text.StringBuilder
        Dim intRet As Integer = 0
        Dim lngPatientAno As Long = 0

        Try
            Call subOutLog("患者常用薬情報登録(subInsPatientJoyoyaku) Start", 0)
            intRet = fncGetPatientAno(objOrder.オーダデータ.患者番号, lngPatientAno, strMsg)

            For lngCounter = 0 To objOrder.患者情報.常用薬.Length - 1
                With objOrder.患者情報.常用薬(lngCounter)
                    strSQL.Clear()
                    strSQL.AppendLine("INSERT INTO TRAN_PATIENT_JOYOYAKU ")
                    strSQL.AppendLine("(")
                    strSQL.AppendLine("PATIENT_ANO,")
                    strSQL.AppendLine("KANJA_NO,")
                    strSQL.AppendLine("JOYOYAKU_NO,")
                    strSQL.AppendLine("JOYOYAKU_NAME,")
                    strSQL.AppendLine("JOYOYAKU_KBN,")
                    strSQL.AppendLine("JOYOYAKU_KBN_NAME ")
                    strSQL.AppendLine(") ")
                    strSQL.AppendLine("VALUES ")
                    strSQL.AppendLine("(")
                    strSQL.AppendLine("" & lngPatientAno & ",")
                    strSQL.AppendLine("'" & objOrder.オーダデータ.患者番号.Trim & "',")
                    strSQL.AppendLine("'" & .常用薬番号 & "',")
                    strSQL.AppendLine("'" & .常用薬名称 & "',")
                    strSQL.AppendLine("'" & .常用薬区分 & "',")
                    strSQL.AppendLine("'" & .常用薬区分名称 & "' ")
                    strSQL.AppendLine(")")
                    '---SQLログ出力をエラー時のみに変更 Deleted By Watanabe 2014.08.06
                    'Call subOutLog("患者常用薬情報登録(subInsPatientJoyoyaku) SQL:" & strSQL.ToString, 0)
                    intRet = fncExecuteNonQuery(objConnect_LocalDB, strSQL.ToString, strMsg)

                End With
            Next lngCounter
        Catch ex As Exception
            strMsg = ex.Message
            Call subOutLog("患者常用薬情報登録(subInsPatientJoyoyaku) エラー：" & strMsg, 1)
            '---SQLログ出力をエラー時のみに変更 ADD By Watanabe 2014.08.06
            If strSQL.ToString <> "" Then
                Call subOutLog("患者常用薬情報登録(subInsPatientJoyoyaku) SQL:" & strSQL.ToString, 1)
            End If
        End Try
    End Sub
#End Region

#Region "患者感染症情報登録(subInsPatientKansen)"
    '**********************************************************************************
    '* 機能概要　：患者感染症情報登録
    '* 関数名称　：subInsPatientKansen
    '* 引　数　　：strMsg    =エラー内容退避用
    '* 作成日付　：2014.02.05
    '* 作成者　　：Created By Watanabe
    '* 更新履歴　：
    '**********************************************************************************
    Private Sub subInsPatientKansen(ByRef strMsg As String)
        Dim lngCounter As Long = 0
        Dim strSQL As New System.Text.StringBuilder
        Dim intRet As Integer = 0
        Dim lngPatientAno As Long = 0

        Try
            Call subOutLog("患者感染症情報登録(subInsPatientKansen) Start", 0)
            intRet = fncGetPatientAno(objOrder.オーダデータ.患者番号, lngPatientAno, strMsg)

            For lngCounter = 0 To objOrder.患者情報.感染症.Length - 1
                With objOrder.患者情報.感染症(lngCounter)
                    strSQL.Clear()
                    strSQL.AppendLine("INSERT INTO TRAN_PATIENT_KANSENSHO ")
                    strSQL.AppendLine("(")
                    strSQL.AppendLine("PATIENT_ANO,")
                    strSQL.AppendLine("KANJA_NO,")
                    strSQL.AppendLine("KANSENSHO_NO,")
                    strSQL.AppendLine("KANSENSHO_NAME,")
                    strSQL.AppendLine("KANSENSHO_KBN,")
                    strSQL.AppendLine("KANSENSHO_KBN_NAME ")
                    strSQL.AppendLine(") ")
                    strSQL.AppendLine("VALUES ")
                    strSQL.AppendLine("(")
                    strSQL.AppendLine("" & lngPatientAno & ",")
                    strSQL.AppendLine("'" & objOrder.オーダデータ.患者番号.Trim & "',")
                    strSQL.AppendLine("'" & .感染症番号 & "',")
                    strSQL.AppendLine("'" & .感染症名称 & "',")
                    strSQL.AppendLine("'" & .感染症区分 & "',")
                    strSQL.AppendLine("'" & .感染症区分名称 & "' ")
                    strSQL.AppendLine(")")
                    '---SQLログ出力をエラー時のみに変更 Deleted By Watanabe 2014.08.06
                    'Call subOutLog("患者感染症情報登録(subInsPatientKansen) SQL:" & strSQL.ToString, 0)
                    intRet = fncExecuteNonQuery(objConnect_LocalDB, strSQL.ToString, strMsg)

                End With
            Next lngCounter
        Catch ex As Exception
            strMsg = ex.Message
            Call subOutLog("患者感染症情報登録(subInsPatientKansen) エラー：" & strMsg, 1)
            '---SQLログ出力をエラー時のみに変更 ADD By Watanabe 2014.08.06
            If strSQL.ToString <> "" Then
                Call subOutLog("患者感染症情報登録(subInsPatientKansen) SQL:" & strSQL.ToString, 1)
            End If
        End Try
    End Sub

#End Region

#Region "患者障害情報登録(subInsPatientShogai)"
    '**********************************************************************************
    '* 機能概要　：患者障害情報登録
    '* 関数名称　：subInsPatientKansen
    '* 引　数　　：strMsg    =エラー内容退避用
    '* 作成日付　：2014.02.05
    '* 作成者　　：Created By Watanabe
    '* 更新履歴　：
    '**********************************************************************************
    Private Sub subInsPatientShogai(ByRef strMsg As String)
        Dim lngCounter As Long = 0
        Dim strSQL As New System.Text.StringBuilder
        Dim intRet As Integer = 0
        Dim lngPatientAno As Long = 0

        Try
            Call subOutLog("患者障害情報登録(subInsPatientShogai) Start", 0)
            intRet = fncGetPatientAno(objOrder.オーダデータ.患者番号, lngPatientAno, strMsg)

            For lngCounter = 0 To objOrder.患者情報.障害情報.Length - 1
                With objOrder.患者情報.障害情報(lngCounter)
                    strSQL.Clear()
                    strSQL.AppendLine("INSERT INTO TRAN_PATIENT_SHOGAI ")
                    strSQL.AppendLine("(")
                    strSQL.AppendLine("PATIENT_ANO,")
                    strSQL.AppendLine("KANJA_NO,")
                    strSQL.AppendLine("SHOGAI_JOHO_NO,")
                    strSQL.AppendLine("SHOGAI_JOHO_NAME,")
                    strSQL.AppendLine("SHOGAI_JOHO_KBN,")
                    strSQL.AppendLine("SHOGAI_JOHO_KBN_NAME ")
                    strSQL.AppendLine(") ")
                    strSQL.AppendLine("VALUES ")
                    strSQL.AppendLine("(")
                    strSQL.AppendLine("" & lngPatientAno & ",")
                    strSQL.AppendLine("'" & objOrder.オーダデータ.患者番号.Trim & "',")
                    strSQL.AppendLine("'" & .障害情報番号 & "',")
                    strSQL.AppendLine("'" & .障害情報名称 & "',")
                    strSQL.AppendLine("'" & .障害情報区分 & "',")
                    strSQL.AppendLine("'" & .障害情報区分名称 & "' ")
                    strSQL.AppendLine(")")
                    '---SQLログ出力をエラー時のみに変更 Deleted By Watanabe 2014.08.06
                    'Call subOutLog("患者障害情報登録(subInsPatientShogai) SQL:" & strSQL.ToString, 0)
                    intRet = fncExecuteNonQuery(objConnect_LocalDB, strSQL.ToString, strMsg)

                End With
            Next lngCounter
        Catch ex As Exception
            strMsg = ex.Message
            Call subOutLog("患者障害情報登録(subInsPatientShogai) エラー：" & strMsg, 1)
            '---SQLログ出力をエラー時のみに変更 ADD By Watanabe 2014.08.06
            If strSQL.ToString <> "" Then
                Call subOutLog("患者障害情報登録(subInsPatientShogai) SQL:" & strSQL.ToString, 1)
            End If
        End Try
    End Sub

#End Region

#End Region

#Region "画面初期処理(subInitForm)"
    '*******************************************************************
    '* 機能概要　：画面初期処理
    '* 関数名称　：subInitForm
    '* 引　数　　：無
    '* 戻り値　　：無
    '* 作成日付　：2011/05/23
    '* 作成者　　：Created By Watanabe
    '* -----------------------------------------------------------------
    '* 更新履歴　：
    '*******************************************************************
    Private Sub subInitForm()
        Me.lstMsg.Items.Clear()                             'リストボックスのクリア
        tmrProc.Interval = My.Settings.PROC_INTERVAL        'TimerIntervalの設定
        tmrProc.Enabled = True                              'Timerの活性化
    End Sub
#End Region

#End Region

#Region "Control Events"

#Region "処理タイマ(tmrProc_Tick)"
    Private Sub tmrProc_Tick(sender As System.Object, e As System.EventArgs) Handles tmrProc.Tick
        tmrProc.Enabled = False
        Call subProcMain()
        tmrProc.Enabled = True
    End Sub
#End Region

#End Region

End Class
