﻿
'///オーダ情報用構造体
Public Class clsGetData
    Public IF_ANO As Integer                'IF番号
    Public UPD_STATUS As String             '更新ステータス
    Public ORDER_STATUS As String           'オーダステータス
    Public ORDER_NUMBER As String           'オーダ番号
    Public DOKUEI_FLG As String             '読影フラグ
    Public PATIENT_ANO As Long              '患者番号
    Public PATIENT_ID As String             '患者ID
    Public PATIENT_KANJI As String          '患者氏名(漢字)
    Public PATIENT_EIJI As String           '患者氏名(ローマ字)
    Public PATIENT_HALF_KANA As String      '患者氏名(半角カナ)
    Public PATIENT_FULL_KANA As String      '患者氏名(全角カナ)
    Public PATIENT_SEX As String            '性別
    Public PATIENT_BIRTH As String          '生年月日
    Public PATIENT_AGE As String            '受診時年齢
    Public NAIGAI_DIV As String             '内外区分(0=健診、1=外来)
    Public DEPT As String                   '診療科
    Public ORDER_DOC As String              '主治医(依頼医)
    Public ORDER_WARD As String             '依頼元病棟
    Public NOW_WARD As String               '現所在病棟
    Public ORDER_DATE As String             '検査予定日
    Public ORDER_TIME As String             '検査予定時刻
    Public ORDER_EXECUTE_DATE As String     '検査実施日
    Public ORDER_EXECUTE_TIME As String     '検査実施時刻
    Public MODALITY As String               'モダリティ
    Public STUDY_NAME As String             '検査種別(検査名)
    Public BODY_PART As String              '検査部位
    Public Filler As String                 '予備
    Public MWM_STATE As String              'MWM進捗(0=未作成、1=作成済)
    Public STUDY_INSTANCE_UID As String     'StudyInstanceUID
End Class

Public Class TYPE_REP_ORDER
    Public REP_ANO As String
    Public REP_STATE As String
    Public ORDER_NO As String
    Public PHOT_DATE As String
    Public PHOT_TIME As String
    Public MODALITY As String
    Public STUDY_UID As String
    Public SERIES_NO As String
    Public PATIENT_ANO As String
    Public NYU_GAI_DIV As String
    Public PATIENT_WARD As String
    Public BODY_PART As String
    Public REQ_DEPT_CD As String
    Public REQ_DOC_NM As String
    Public REQ_COMMENT As String
    Public KENSA_MOKUTEKI As String
    Public REP_DIV As String
    Public REP_ENG As String
    Public REP_ENG_COMMENT As String
    Public REP_ENG_SEARCH_CMT As String
    Public REP_DOC_COMMENT As String
    Public REP_1ST_DOC As String
    Public REP_1ST_DATE As String
    Public REP_1ST_TIME As String
    Public REP_2ND_DOC As String
    Public REP_2ND_DATE As String
    Public REP_2ND_TIME As String
    Public REP_DIAGNOSIS As String
    Public REP_OPINION As String
    Public PRINT_STATE As String
    Public KENSA_ORDER_ID As String
End Class

Public Class TYPE_REP_OPINION
    Public REP_ANO As String
    Public REP_DOC_HIST As String
    Public BODY_PART As String
    Public REP_OPINION1 As String
    Public REP_OPINION2 As String
    Public REP_OPINION3 As String
    Public REP_OPINION4 As String
    Public REP_OPINION5 As String
    Public REP_OPINION6 As String
    Public REP_OPINION7 As String
    Public REP_OPINION8 As String
    Public REP_OPINION9 As String
    Public REP_OPINION10 As String
    Public REP_IMAGE_PATH1 As String
    Public REP_IMAGE_PATH2 As String
    Public REP_IMAGE_PATH3 As String
    Public REP_IMAGE_PATH4 As String
    Public REP_IMAGE_PATH5 As String
    Public REP_DIAGNOSIS As String
    Public REP_ENG As String
    Public REP_DOC As String
    Public ENG_COMMENT As String
    Public REP_ENG_SEARCH_CMT As String
    Public DOC_COMMENT As String
    Public PRINT_STATE As String
    Public INS_DATE As String
    Public INS_TIME As String
    Public INS_USER As String
    Public UPD_DATE As String
    Public UPD_TIME As String
    Public UPD_USER As String
    Public DEL_FLG As String
End Class

Public Class TYPE_REP_IMAGE
    Public REP_ANO As String
    Public REP_DOC_HIST As String
    Public REP_IMAGE As String
    Public REP_IMAGE_PATH As String
    Public REP_IMAGE_SORT As String
    Public IMAGE_DATA As Image
    Public INS_DATE As String
    Public INS_TIME As String
    Public INS_USER As String
    Public UPD_DATE As String
    Public UPD_TIME As String
    Public UPD_USER As String
    Public DEL_FLG As String
End Class

