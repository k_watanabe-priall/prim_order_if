﻿Imports System
Imports System.Drawing
Imports System.Drawing.Imaging

Friend Class Image2Jpeg

    ''' <summary>
    '''  Image -> JPEGﾌｧｲﾙ 変換処理
    ''' </summary>
    ''' <param name="IamageObcect"></param>
    ''' <param name="FilePath"></param>
    ''' <param name="Quality"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function FromJpegImage(ByVal IamageObcect As Image, ByVal FilePath As String, Optional ByVal Quality As Int64 = 100) As Image

        Dim retImage As Image = Nothing

        SaveJpengFiles(IamageObcect, FilePath, Quality)

        retImage = FromFileImage(FilePath)

        Return retImage

    End Function

    ''' <summary>
    '''  JPEGﾌｧｲﾙ -> Image 変換処理
    ''' </summary>
    ''' <param name="FilePath"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Shared Function FromFileImage(ByVal FilePath As String) As Image

        Dim fs As System.IO.FileStream

        fs = New System.IO.FileStream(FilePath, IO.FileMode.Open, IO.FileAccess.Read)

        Dim retImage As Bitmap = CType(System.Drawing.Image.FromStream(fs), Bitmap)

        fs.Close()

        fs.Dispose()

        Return New Bitmap(retImage)

    End Function

    ''' <summary>
    '''  ImageObject -> Jpeg形式ﾌｧｲﾙ 変換処理
    ''' </summary>
    ''' <param name="IamageObcect"></param>
    ''' <param name="FilePath"></param>
    ''' <param name="Quality"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function SaveJpengFiles(ByVal IamageObcect As Image, ByVal FilePath As String, Optional ByVal Quality As Int64 = 100) As Boolean

        Dim jpgEncoder As ImageCodecInfo = Nothing

        'JPEG用のｴﾝｺｰﾀﾞの取得
        For Each ici As ImageCodecInfo In ImageCodecInfo.GetImageEncoders()
            If ici.FormatID = ImageFormat.Jpeg.Guid Then
                jpgEncoder = ici
                Exit For
            End If
        Next

        'ｴﾝｺｰﾀﾞに渡すﾊﾟﾗﾒｰﾀの作成
        Dim encParam As New EncoderParameter(Encoder.Quality, Quality)

        'パラメータを配列に格納
        Dim encParams As New EncoderParameters(1)
        encParams.Param(0) = encParam

        '画像の保存
        IamageObcect.Save(FilePath, jpgEncoder, encParams)

        Return True

    End Function

End Class
