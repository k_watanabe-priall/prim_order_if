﻿''' <summary>
''' レポート移行データ構造体
''' </summary>
''' <remarks>
''' </remarks>
<System.Xml.Serialization.XmlRoot("reg")> _
Public Class typeReportData
    <System.Xml.Serialization.XmlElement("diagnosis_grp")> _
    Public ﾚﾎﾟｰﾄ情報用構造体 As New typeDiagnosisGrp
End Class

''' <summary>
''' ﾚﾎﾟｰﾄ情報用構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typeDiagnosisGrp
    <System.Xml.Serialization.XmlElement("patient_information")> _
    Public 患者情報 As New typePatientInfomation

    <System.Xml.Serialization.XmlElement("request_information")> _
    Public 依頼情報 As New typeRequestInformation

    <System.Xml.Serialization.XmlElement("study_information")> _
    Public 検査情報 As New typeStudyInformation

    <System.Xml.Serialization.XmlElement("diagnosis_information")> _
    Public 読影情報 As New typeDiagnosisInformation
End Class

''' <summary>
''' 患者情報用構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typePatientInfomation
    <System.Xml.Serialization.XmlElement("patient_info")> _
    Public 患者詳細情報 As New typePatientInfo

    <System.Xml.Serialization.XmlElement("patient_sub_info")> _
    Public 患者補助情報 As New typePatientSubInfo
End Class

''' <summary>
''' 患者詳細情報用構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typePatientInfo
    <System.Xml.Serialization.XmlElement("patient_id")> _
    Public 患者ID As String

    <System.Xml.Serialization.XmlElement("kanji_name")> _
    Public 漢字氏名 As String

    <System.Xml.Serialization.XmlElement("kana_name")> _
    Public カナ氏名 As String

    <System.Xml.Serialization.XmlElement("roma_name")> _
    Public ローマ字氏名 As String

    <System.Xml.Serialization.XmlElement("birthday")> _
    Public 生年月日 As String

    <System.Xml.Serialization.XmlElement("sex")> _
    Public 性別 As New typeSex
End Class

''' <summary>
''' 性別用構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typeSex
    <System.Xml.Serialization.XmlElement("code")> _
    Public 性別コード As String

    <System.Xml.Serialization.XmlElement("long_name")> _
    Public 性別正式名称 As String

    <System.Xml.Serialization.XmlElement("short_name")> _
    Public 性別略名称 As String
End Class

''' <summary>
''' 患者補助情報用構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typePatientSubInfo
    <System.Xml.Serialization.XmlElement("anamnesis")> _
    Public 既往歴 As String
End Class

''' <summary>
''' 依頼情報用構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typeRequestInformation
    <System.Xml.Serialization.XmlElement("request_info")> _
    Public 依頼詳細情報 As New typeRequestInfo
End Class

''' <summary>
''' 依頼詳細情報用構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typeRequestInfo
    <System.Xml.Serialization.XmlElement("report_flag")> _
    Public 読影有無フラグ As String

    <System.Xml.Serialization.XmlElement("order_date")> _
    Public 検査依頼日 As String

    <System.Xml.Serialization.XmlElement("schedule_date")> _
    Public 検査予定日 As String

    <System.Xml.Serialization.XmlElement("reception_date")> _
    Public 検査受付日 As String

End Class

''' <summary>
''' 検査情報用構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typeStudyInformation
    <System.Xml.Serialization.XmlElement("exam_info")> _
    Public 実施情報 As New typeExamInformation

    <System.Xml.Serialization.XmlElement("exam_sub_info")> _
    Public 実施補助情報 As New typeExamSubInfo

End Class

''' <summary>
''' 実施情報用構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typeExamInformation
    <System.Xml.Serialization.XmlElement("order_no")> _
    Public オーダ番号 As String

    <System.Xml.Serialization.XmlElement("exam_date")> _
    Public 検査実施日 As String

    <System.Xml.Serialization.XmlElement("class")> _
    Public 検査種別明細 As New typeClass

    <System.Xml.Serialization.XmlElement("modality")> _
    Public モダリティ As New typeModality

    <System.Xml.Serialization.XmlElement("urgent")> _
    Public 緊急区分情報 As New typeUrgent

    <System.Xml.Serialization.XmlElement("ent_out")> _
    Public 入外区分情報 As New typeEntOut

    <System.Xml.Serialization.XmlElement("ward")> _
    Public 病棟情報 As New typeWard

    <System.Xml.Serialization.XmlElement("request")> _
    Public 依頼科情報 As New typeWard

    <System.Xml.Serialization.XmlElement("req_doctor")> _
    Public 依頼医情報 As New typeRecDoc

    <System.Xml.Serialization.XmlElement("part")> _
    Public 部位情報 As New typePart

    <System.Xml.Serialization.XmlElement("drug")> _
    Public 薬剤情報 As New typeDrug

End Class

''' <summary>
''' 検査種別用構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typeClass
    <System.Xml.Serialization.XmlElement("code")> _
    Public 検査種別コード As String

    <System.Xml.Serialization.XmlElement("long_name")> _
    Public 検査種別名称 As String

    <System.Xml.Serialization.XmlElement("short_name")> _
    Public 検査種別略称 As String
End Class

''' <summary>
''' モダリティ用構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typeModality
    <System.Xml.Serialization.XmlElement("code")> _
    Public モダリティコード As String
End Class

''' <summary>
''' 緊急用構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typeUrgent
    <System.Xml.Serialization.XmlElement("code")> _
    Public 緊急区分コード As String

    <System.Xml.Serialization.XmlElement("long_name")> _
    Public 緊急区分名称 As String

    <System.Xml.Serialization.XmlElement("short_name")> _
    Public 緊急区分略称 As String
End Class

''' <summary>
''' 入外用構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typeEntOut
    <System.Xml.Serialization.XmlElement("code")> _
    Public 入外区分コード As String

    <System.Xml.Serialization.XmlElement("long_name")> _
    Public 入外区分名称 As String

    <System.Xml.Serialization.XmlElement("short_name")> _
    Public 入外区分略称 As String
End Class

''' <summary>
''' 病棟用構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typeWard
    <System.Xml.Serialization.XmlElement("code")> _
    Public 病棟コード As String

    <System.Xml.Serialization.XmlElement("long_name")> _
    Public 病棟名称 As String

    <System.Xml.Serialization.XmlElement("short_name")> _
    Public 病棟略称 As String
End Class

''' <summary>
''' 依頼科用構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typeRequest
    <System.Xml.Serialization.XmlElement("code")> _
    Public 依頼科コード As String

    <System.Xml.Serialization.XmlElement("long_name")> _
    Public 依頼科名称 As String

    <System.Xml.Serialization.XmlElement("short_name")> _
    Public 依頼科略称 As String
End Class

''' <summary>
''' 依頼医師用構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typeRecDoc
    <System.Xml.Serialization.XmlElement("code")> _
    Public 依頼医コード As String

    <System.Xml.Serialization.XmlElement("long_name")> _
    Public 依頼医名称 As String

    <System.Xml.Serialization.XmlElement("short_name")> _
    Public 依頼医略称 As String
End Class

''' <summary>
''' 部位用構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typePart
    <System.Xml.Serialization.XmlElement("code")> _
    Public 部位コード As String

    <System.Xml.Serialization.XmlElement("long_name")> _
    Public 依部位名称 As String

    <System.Xml.Serialization.XmlElement("short_name")> _
    Public 部位略称 As String
End Class

''' <summary>
''' 薬剤用構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typeDrug
    <System.Xml.Serialization.XmlElement("code")> _
    Public 薬剤コード As String

    <System.Xml.Serialization.XmlElement("long_name")> _
    Public 薬剤名称 As String

    <System.Xml.Serialization.XmlElement("short_name")> _
    Public 薬剤略称 As String
End Class

''' <summary>
''' 実施補助情報用構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typeExamSubInfo
    <System.Xml.Serialization.XmlElement("referral_hospital")> _
    Public 紹介病院 As New typeReferralHospital

    <System.Xml.Serialization.XmlElement("infection_comment")> _
    Public 感染禁忌コメント As String

    <System.Xml.Serialization.XmlElement("recording_comment")> _
    Public 実施コメント As String

    <System.Xml.Serialization.XmlElement("reading_comment")> _
    Public 読影コメント As String

    <System.Xml.Serialization.XmlElement("detail_comment")> _
    Public 明細コメント As String

    <System.Xml.Serialization.XmlElement("comment_1")> _
    Public コメント1 As String

    <System.Xml.Serialization.XmlElement("comment_2")> _
    Public コメント2 As String

    <System.Xml.Serialization.XmlElement("comment_3")> _
    Public コメント3 As String

    <System.Xml.Serialization.XmlElement("comment_4")> _
    Public 想定病名 As String

    <System.Xml.Serialization.XmlElement("comment_5")> _
    Public コメント5 As String

    <System.Xml.Serialization.XmlElement("comment_6")> _
    Public 依頼科コメント As String

    <System.Xml.Serialization.XmlElement("comment_7")> _
    Public 放射線科コメント As String

    <System.Xml.Serialization.XmlElement("comment_8")> _
    Public 検査目的 As String

End Class

''' <summary>
''' 紹介病院情報用構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typeReferralHospital
    <System.Xml.Serialization.XmlElement("code")> _
    Public 紹介病院コード As String
End Class

''' <summary>
''' 読影情報用構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typeDiagnosisInformation
    <System.Xml.Serialization.XmlElement("report_info")> _
    Public 読影情報 As New typeDiagnosisInfo
End Class

''' <summary>
''' 読影情報用構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typeDiagnosisInfo
    <System.Xml.Serialization.XmlElement("report_status")> _
    Public 読影詳細情報 As New typeReportStatus

    <System.Xml.Serialization.XmlElement("finding_1")> _
    Public 所見情報 As New typeOpinion

    <System.Xml.Serialization.XmlElement("finding_2")> _
    Public Impession情報 As New typeImpression

    <System.Xml.Serialization.XmlElement("finding_3")> _
    Public コメント3情報 As New typeComment

    <System.Xml.Serialization.XmlElement("finding_4")> _
    Public コメント4情報 As New typeComment

    <System.Xml.Serialization.XmlElement("finding_5")> _
    Public コメント5情報 As New typeComment

    <System.Xml.Serialization.XmlElement("finding_6")> _
    Public 放射線科メモ As New typeComment

    <System.Xml.Serialization.XmlElement("key_image")> _
    Public キー画像 As New typeKeyImage

End Class

''' <summary>
''' 読影状況用構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typeReportStatus
    <System.Xml.Serialization.XmlElement("report_id")> _
    Public 読影ステータス As String

    <System.Xml.Serialization.XmlElement("observe_date")> _
    Public レポート確定日時 As String

    <System.Xml.Serialization.XmlElement("observer")> _
    Public 所属情報 As New typeObserver


End Class

''' <summary>
''' 所属用構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typeObserver
    <System.Xml.Serialization.XmlElement("hospital")> _
    Public 所属病院情報 As New typeHospital

    <System.Xml.Serialization.XmlElement("doctor")> _
    Public 所属医師情報 As New typeDoctor
End Class

''' <summary>
''' 所属病院構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typeHospital
    <System.Xml.Serialization.XmlElement("code")> _
    Public 所属病院コード As String

    <System.Xml.Serialization.XmlElement("long_name")> _
    Public 所属病院名称 As String

    <System.Xml.Serialization.XmlElement("short_name")> _
    Public 所属病院略称 As String
End Class

''' <summary>
''' 所属医師構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typeDoctor
    <System.Xml.Serialization.XmlElement("code")> _
    Public 読影医コード As String

    <System.Xml.Serialization.XmlElement("long_name")> _
    Public 読影医名称 As String

    <System.Xml.Serialization.XmlElement("short_name")> _
    Public 読影医略称 As String
End Class

''' <summary>
''' 所見用構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typeOpinion
    <System.Xml.Serialization.XmlElement("content")> _
    Public 所見 As String
End Class

''' <summary>
'''Impression用構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typeImpression
    <System.Xml.Serialization.XmlElement("content")> _
    Public Impession As String
End Class

''' <summary>
'''コメント用構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typeComment
    <System.Xml.Serialization.XmlElement("content")> _
    Public Comment3 As String
End Class

''' <summary>
'''KeyImage用構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typeKeyImage
    <System.Xml.Serialization.XmlElement("filename")> _
    Public キー画像ファイル名() As String
End Class

'///////////////////////// 湘南第一病院 様向け HAPPY I/F用 /////////////////////////
''' <summary>
''' オーダ構造体
''' </summary>
''' <remarks>
''' </remarks>
<System.Xml.Serialization.XmlRoot("ORDER")> _
Public Class typeOrderData
    <System.Xml.Serialization.XmlElement("HEADER")> _
    Public ヘッダ As New typeHeader

    <System.Xml.Serialization.XmlElement("KUGIRI")> _
    Public オーダデータ As New typeOrderInf

    <System.Xml.Serialization.XmlElement("KANJA_JOHO")> _
    Public 患者情報 As New typePatient

    <System.Xml.Serialization.XmlElement("NYUTAIIN_JOHO")> _
    Public 入退院情報 As New typeNyutaiin

End Class

''' <summary>
'''Header構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typeHeader
    <System.Xml.Serialization.XmlElement("FORMAT_NAME")> _
    Public フォーマット名 As String

    <System.Xml.Serialization.XmlElement("MAJOR_VERSION")> _
    Public メジャーバージョン As String

    <System.Xml.Serialization.XmlElement("MINOR_VERSION")> _
    Public マイナーバージョン As String

    <System.Xml.Serialization.XmlElement("SEND_FROM")> _
    Public 送信元 As String

    <System.Xml.Serialization.XmlElement("SEND_TO")> _
    Public 送信先 As String
End Class

''' <summary>
'''オーダデータ構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typeOrderInf
    <System.Xml.Serialization.XmlElement("KANJA_NO")> _
    Public 患者番号 As String

    <System.Xml.Serialization.XmlElement("SHIJI_DATE")> _
    Public 指示日 As String

    <System.Xml.Serialization.XmlElement("SYNONYM")> _
    Public シノニム As String

    <System.Xml.Serialization.XmlElement("SHINRYO_KBN")> _
    Public 診療区分 As String

    <System.Xml.Serialization.XmlElement("KUGIRI_NO")> _
    Public 区切番号 As String

    <System.Xml.Serialization.XmlElement("SHINRYO_KBN_NAME")> _
    Public 診療区分名称 As String

    <System.Xml.Serialization.XmlElement("SHINRYO_KBN_RNAME")> _
    Public 診療区分略称 As String

    <System.Xml.Serialization.XmlElement("KINKYU_KBN")> _
    Public 緊急区分 As String

    <System.Xml.Serialization.XmlElement("KINKYU_KBN_NAME")> _
    Public 緊急区分名称 As String

    <System.Xml.Serialization.XmlElement("JIGO_NYURYOKU_FLG")> _
    Public 事後入力フラグ As String

    <System.Xml.Serialization.XmlElement("JIGO_NYURYOKU_FLG_NAME")> _
    Public 事後入力フラグ名称 As String

    <System.Xml.Serialization.XmlElement("HOKEN_SHUBETSU")> _
    Public 保険種別 As String

    <System.Xml.Serialization.XmlElement("HOKEN_SHUBETSU_NAME")> _
    Public 保険種別名称 As String

    <System.Xml.Serialization.XmlElement("SHINKI_FLG")> _
    Public 新規フラグ As String

    <System.Xml.Serialization.XmlElement("SHINKI_FLG_NAME")> _
    Public 新規フラグ名称 As String

    <System.Xml.Serialization.XmlElement("KENSA_SHUBETSU")> _
    Public 検査種別 As String

    <System.Xml.Serialization.XmlElement("KENSA_SHUBETSU_NAME")> _
    Public 検査種別名称 As String

    <System.Xml.Serialization.XmlElement("SHOHO_KBN")> _
    Public 処方区分 As String

    <System.Xml.Serialization.XmlElement("SHOHO_KBN_NAME")> _
    Public 処方区分名称 As String

    <System.Xml.Serialization.XmlElement("TEIRINJI_FLG")> _
    Public 定臨時フラグ As String

    <System.Xml.Serialization.XmlElement("TEIRINJI_FLG_NAME")> _
    Public 定臨時フラグ名称 As String

    <System.Xml.Serialization.XmlElement("NICHI_KAISU")> _
    Public 日回数 As String

    <System.Xml.Serialization.XmlElement("JISSHIYOTEI_KAISHI_DATETIME")> _
    Public 実施予定開始日時 As String

    <System.Xml.Serialization.XmlElement("JISSHIYOTEI_KAISHI_JIKAN_KBN")> _
    Public 実施予定開始時間区分 As String

    <System.Xml.Serialization.XmlElement("JISSHIYOTEI_KAISHI_JIKAN_KBN_NAME")> _
    Public 実施予定開始時間区分名称 As String

    <System.Xml.Serialization.XmlElement("JISSHIYOTEI_DATE_KANKAKU")> _
    Public 実施予定日間隔 As String

    <System.Xml.Serialization.XmlElement("JISSHIYOTEI_SHURYO_DATE")> _
    Public 実施予定終了日付 As String

    <System.Xml.Serialization.XmlElement("JISSHIYOTEI_SHURYO_JIKAN_KBN")> _
    Public 実施予定終了時間区分 As String

    <System.Xml.Serialization.XmlElement("JISSHIYOTEI_SHURYO_JIKAN_KBN_NAME")> _
    Public 実施予定終了時間区分名称 As String

    <System.Xml.Serialization.XmlElement("ORDER_NO")> _
    Public オーダ番号 As String

    <System.Xml.Serialization.XmlElement("CHUSHI_HAMBETSU_FLG")> _
    Public 中止判別フラグ As String

    <System.Xml.Serialization.XmlElement("CHUSHI_DATETIME")> _
    Public 中止日時 As String

    <System.Xml.Serialization.XmlElement("CHUSHI_JIKAN_KBN")> _
    Public 中止時間区分 As String

    <System.Xml.Serialization.XmlElement("CHUSHI_JIKAN_KBN_NAME")> _
    Public 中止時間区分名称 As String

    <System.Xml.Serialization.XmlElement("BUMONKA_CODE")> _
    Public 部門科コード As String

    <System.Xml.Serialization.XmlElement("BUMONKA_NAME")> _
    Public 部門科名称 As String

    <System.Xml.Serialization.XmlElement("FREE_COMMENT")> _
    Public フリーコメント As String

    <System.Xml.Serialization.XmlElement("ORDER_STATUS")> _
    Public オーダステータス As String

    <System.Xml.Serialization.XmlElement("HUJISSHI_JOHO_DATETIME")> _
    Public 不実施情報日時 As String

    <System.Xml.Serialization.XmlElement("HUJISSHI_JOHO_SOSASHA")> _
    Public 不実施情報操作者 As New typePersonal

    <System.Xml.Serialization.XmlElement("UKETSUKE_DATETIME")> _
    Public 受付日時 As String

    <System.Xml.Serialization.XmlElement("UKETSUKE_JOHO_SOSASHA")> _
    Public 受付操作者 As New typePersonal

    <System.Xml.Serialization.XmlElement("JISSHI_DATETIME")> _
    Public 実施日時 As String

    <System.Xml.Serialization.XmlElement("JISSHI_SOSASHA")> _
    Public 実施操作者 As New typePersonal

    <System.Xml.Serialization.XmlElement("SAKUJO_DATETIME")> _
    Public 削除日時 As String

    <System.Xml.Serialization.XmlElement("SAKUJO_SOSASHA")> _
    Public 削除操作者 As New typePersonal

    <System.Xml.Serialization.XmlElement("CHUSHI_SOSA_DATETIME")> _
    Public 中止操作日時 As String

    <System.Xml.Serialization.XmlElement("CHUSHI_SOSASHA")> _
    Public 中止操作者 As New typePersonal

    <System.Xml.Serialization.XmlElement("CHUSHI_SHIJII")> _
    Public 中止指示医 As New typePersonal

    <System.Xml.Serialization.XmlElement("HOSOKU_NYURYOKU_DATETIME")> _
    Public 補足入力日時 As String

    <System.Xml.Serialization.XmlElement("HOSOKU_NYURYOKU_SOSASHA")> _
    Public 補足入力操作者 As New typePersonal

    <System.Xml.Serialization.XmlElement("KENSA_KEKKA_UMU")> _
    Public 検査結果有無 As String

    <System.Xml.Serialization.XmlElement("KENSA_KEKKA_UMU_NAME")> _
    Public 検査結果有無名称 As String

    <System.Xml.Serialization.XmlElement("SHOKUJI_SESSHU_YOHI")> _
    Public 食事摂取要否 As String

    <System.Xml.Serialization.XmlElement("SHOKUJI_SESSHU_YOHI_NAME")> _
    Public 食事摂取要否名称 As String

    <System.Xml.Serialization.XmlElement("KENSA_REPORT_UMU")> _
    Public 検査レポート有無 As String

    <System.Xml.Serialization.XmlElement("KENSA_REPORT_UMU_NAME")> _
    Public 検査レポート有無名称 As String

    <System.Xml.Serialization.XmlElement("SHONIN_YOHI")> _
    Public 承認要否 As String

    <System.Xml.Serialization.XmlElement("SHONIN_YOHI_NAME")> _
    Public 承認要否名称 As String

    <System.Xml.Serialization.XmlElement("KOHI_JOHO_REMBAN1")> _
    Public 公費情報連番1 As String

    <System.Xml.Serialization.XmlElement("KOHI_JOHO_REMBAN1_NAME")> _
    Public 公費情報連番1名称 As String

    <System.Xml.Serialization.XmlElement("KOHI_JOHO_REMBAN2")> _
    Public 公費情報連番2 As String

    <System.Xml.Serialization.XmlElement("KOHI_JOHO_REMBAN2_NAME")> _
    Public 公費情報連番2名称 As String

    <System.Xml.Serialization.XmlElement("KANRI_JOHO")> _
    Public 管理情報 As New typeKanriInf

    <System.Xml.Serialization.XmlElement("MEISAI")> _
    Public 明細() As typeMeisai

    <System.Xml.Serialization.XmlElement("SHOSAI")> _
    Public 詳細 As New typeShosai

    '<System.Xml.Serialization.XmlElement("URL")> _
    'Public URL As New typeShosai

    '<System.Xml.Serialization.XmlElement("URL2")> _
    'Public URL2 As New typeShosai

    '<System.Xml.Serialization.XmlElement("DENGON_MEMO")> _
    'Public 伝言メモ As New typeShosai

    '<System.Xml.Serialization.XmlElement("SATELLITE_FLG")> _
    'Public サテライトフラグ As String

    '<System.Xml.Serialization.XmlElement("KANJA_JOHO")> _
    'Public 患者情報 As New typePatient

    '<System.Xml.Serialization.XmlElement("NYUTAIIN_JOHO")> _
    'Public 入退院情報 As New typeNyutaiin

End Class

''' <summary>
'''職員情報構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typePersonal
    <System.Xml.Serialization.XmlElement("SHOKUIN")> _
    Public 職員情報 As New typePersonalInf
End Class

''' <summary>
'''オーダデータ職員情報構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typePersonalInf
    <System.Xml.Serialization.XmlElement("SHOKUIN_ID")> _
    Public 職員ID As String

    <System.Xml.Serialization.XmlElement("SHOKUIN_NAME")> _
    Public 職員氏名 As String

End Class

''' <summary>
'''管理情報構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typeKanriInf
    <System.Xml.Serialization.XmlElement("NYUGAI_KBN")> _
    Public 入外区分 As String

    <System.Xml.Serialization.XmlElement("NYUGAI_KBN_NAME")> _
    Public 入外区分名称 As String

    <System.Xml.Serialization.XmlElement("SHINRYOKA_CODE")> _
    Public 診療科コード As String

    <System.Xml.Serialization.XmlElement("SHINRYOKA_NAME")> _
    Public 診療科名称 As String

    <System.Xml.Serialization.XmlElement("BUMON_CODE")> _
    Public 部門コード As String

    <System.Xml.Serialization.XmlElement("BUMON_NAME")> _
    Public 部門名称 As String

    <System.Xml.Serialization.XmlElement("SHINSATSUSHITSU_NO")> _
    Public 診察室番号 As String

    <System.Xml.Serialization.XmlElement("SHINSATSUSHITSU_NAME")> _
    Public 診察室名称 As String

    <System.Xml.Serialization.XmlElement("SOSASYA")> _
    Public 操作者 As New typePersonal

    <System.Xml.Serialization.XmlElement("SHIJII")> _
    Public 指示医 As New typePersonal

End Class

''' <summary>
'''管理情報構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typeMeisai
    <System.Xml.Serialization.XmlElement("MEISAI_NO")> _
    Public 明細番号 As String

    <System.Xml.Serialization.XmlElement("NURYOKU_KBN")> _
    Public 入力区分 As String

    <System.Xml.Serialization.XmlElement("NURYOKU_KBN_NAME")> _
    Public 入力区分名称 As String

    <System.Xml.Serialization.XmlElement("KOMOKU_HIMBAN")> _
    Public 項目品番 As String

    <System.Xml.Serialization.XmlElement("KOMOKU_NAME")> _
    Public 項目名称 As String

    <System.Xml.Serialization.XmlElement("SURYO")> _
    Public 数量 As String

    <System.Xml.Serialization.XmlElement("TANI")> _
    Public 単位 As String

    <System.Xml.Serialization.XmlElement("SHIJI_COMMENT")> _
    Public 指示コメント As String

End Class

''' <summary>
'''詳細情報構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typeShosai
    <System.Xml.Serialization.XmlElement("FRAME")> _
    Public フレーム() As typeFrame

    <System.Xml.Serialization.XmlElement("SCHEMA_COMMENT")> _
    Public シェーマコメント As String

End Class

''' <summary>
'''フレーム情報構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typeFrame
    <System.Xml.Serialization.XmlElement("FRAME_NO")> _
    Public フレーム番号 As String

    <System.Xml.Serialization.XmlElement("FRAME_NAME")> _
    Public フレーム名称 As String

    <System.Xml.Serialization.XmlElement("SHOSAI_KOMOKU")> _
    Public 詳細項目() As typeShosaiKoumoku

End Class

''' <summary>
'''詳細項目情報構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typeShosaiKoumoku
    <System.Xml.Serialization.XmlElement("ITEM_NO")> _
    Public 項目番号 As String

    <System.Xml.Serialization.XmlElement("NAME")> _
    Public 項目名称 As String

    <System.Xml.Serialization.XmlElement("VALUE")> _
    Public 登録内容 As String

End Class

''' <summary>
'''患者情報構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typePatient
    <System.Xml.Serialization.XmlElement("KANJA_NO")> _
    Public 患者番号 As String

    <System.Xml.Serialization.XmlElement("KANJI_NAME")> _
    Public 漢字氏名 As String

    <System.Xml.Serialization.XmlElement("KANA_NAME")> _
    Public カナ氏名 As String

    <System.Xml.Serialization.XmlElement("SEIBETSU")> _
    Public 性別 As String

    <System.Xml.Serialization.XmlElement("SEIBETSU_NAME")> _
    Public 性別名称 As String

    <System.Xml.Serialization.XmlElement("ADDRESS")> _
    Public 住所 As String

    <System.Xml.Serialization.XmlElement("SHINCHO")> _
    Public 身長 As String

    <System.Xml.Serialization.XmlElement("TAIJYU")> _
    Public 体重 As String

    <System.Xml.Serialization.XmlElement("BIRTH_DATE")> _
    Public 生年月日 As String

    <System.Xml.Serialization.XmlElement("YUBIN_NO")> _
    Public 郵便番号 As String

    <System.Xml.Serialization.XmlElement("TEL_NO")> _
    Public 電話番号 As String

    <System.Xml.Serialization.XmlElement("RENRAKUSAKI_TEL_NO")> _
    Public 連絡先電話番号 As String

    <System.Xml.Serialization.XmlElement("OLD_KANA_NAME")> _
    Public 旧カナ氏名 As String

    <System.Xml.Serialization.XmlElement("OLD_KANJI_NAME")> _
    Public 旧漢字氏名 As String

    <System.Xml.Serialization.XmlElement("OLD_NAME_YUKOKIGEN")> _
    Public 旧氏名有効期限 As String

    <System.Xml.Serialization.XmlElement("BLOOD_TYPE_ABO")> _
    Public 血液型ABO As String

    <System.Xml.Serialization.XmlElement("BLOOD_TYPE_RH")> _
    Public 血液型RH As String

    <System.Xml.Serialization.XmlElement("BLOOD_TYPE_DATE")> _
    Public 血液型最終確認日 As String

    <System.Xml.Serialization.XmlElement("NINSHIN_UMU")> _
    Public 妊娠有無 As String

    <System.Xml.Serialization.XmlElement("NINSHIN_UMU_NAME")> _
    Public 妊娠有無名称 As String

    <System.Xml.Serialization.XmlElement("BUNBEN_YOTEI_DATE")> _
    Public 分娩予定日 As String

    <System.Xml.Serialization.XmlElement("NINSHIN_SHUSU")> _
    Public 妊娠週数 As String

    <System.Xml.Serialization.XmlElement("HOKOU")> _
    Public 歩行状態 As String

    <System.Xml.Serialization.XmlElement("HOKOU_NAME")> _
    Public 歩行状態名称 As String

    <System.Xml.Serialization.XmlElement("ANSEIDO")> _
    Public 安静度 As String

    <System.Xml.Serialization.XmlElement("ANSEIDO_NAME")> _
    Public 安静度名称 As String

    <System.Xml.Serialization.XmlElement("KANGODO")> _
    Public 看護度 As String

    <System.Xml.Serialization.XmlElement("SHOKUJI_KAIJYODO")> _
    Public 食事介助度 As String

    <System.Xml.Serialization.XmlElement("SHOKUJI_KAIJYODO_NAME")> _
    Public 食事介助度名称 As String

    <System.Xml.Serialization.XmlElement("HUKUYAKU_SHIDO")> _
    Public 服薬指導 As String

    <System.Xml.Serialization.XmlElement("HUKUYAKU_SHIDO_NAME")> _
    Public 服薬指導名称 As String

    <System.Xml.Serialization.XmlElement("NINI_JOHO")> _
    Public 任意情報 As String

    <System.Xml.Serialization.XmlElement("NINI_JOHO_NAME")> _
    Public 任意情報名称 As String

    <System.Xml.Serialization.XmlElement("SHOGAI_JOHO")> _
    Public 障害情報() As typeShogaiInf

    <System.Xml.Serialization.XmlElement("JOYOYAKU")> _
    Public 常用薬() As typeJyoyoyaku

    <System.Xml.Serialization.XmlElement("KANSENSHO")> _
    Public 感染症() As typeKansensho


End Class

''' <summary>
'''入退院情報構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typeNyutaiin
    <System.Xml.Serialization.XmlElement("NYUIN_DATE")> _
    Public 入院日 As String

    <System.Xml.Serialization.XmlElement("TAIIN_DATE")> _
    Public 退院日 As String

    <System.Xml.Serialization.XmlElement("BYOU_CODE")> _
    Public 病棟コード As String

    <System.Xml.Serialization.XmlElement("BYOU_NAME")> _
    Public 病棟名称 As String

    <System.Xml.Serialization.XmlElement("BYOUSHITSU_NO")> _
    Public 病室番号 As String

    <System.Xml.Serialization.XmlElement("BYOUSHITSU_NAME")> _
    Public 病室名称 As String

    <System.Xml.Serialization.XmlElement("NYUIN_KA")> _
    Public 入院科 As String

    <System.Xml.Serialization.XmlElement("NYUIN_KA_NAME")> _
    Public 入院科名称 As String

    <System.Xml.Serialization.XmlElement("SAISHU_TENTO_DATE")> _
    Public 最終転棟転室日 As String

    <System.Xml.Serialization.XmlElement("SHUJII")> _
    Public 主治医 As New typePersonalInf

End Class

''' <summary>
'''障害情報構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typeShogaiInf
    <System.Xml.Serialization.XmlElement("SHOGAI_JOHO_NO")> _
    Public 障害情報番号 As String

    <System.Xml.Serialization.XmlElement("SHOGAI_JOHO_NAME")> _
    Public 障害情報名称 As String

    <System.Xml.Serialization.XmlElement("SHOGAI_JOHO_KBN")> _
    Public 障害情報区分 As String

    <System.Xml.Serialization.XmlElement("SHOGAI_JOHO_KBN_NAME")> _
    Public 障害情報区分名称 As String

End Class

''' <summary>
'''常用薬構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typeJyoyoyaku
    <System.Xml.Serialization.XmlElement("JOYOYAKU_NO")> _
    Public 常用薬番号 As String

    <System.Xml.Serialization.XmlElement("JOYOYAKU_NAME")> _
    Public 常用薬名称 As String

    <System.Xml.Serialization.XmlElement("JOYOYAKU_KBN")> _
    Public 常用薬区分 As String

    <System.Xml.Serialization.XmlElement("JOYOYAKU_KBN_NAME")> _
    Public 常用薬区分名称 As String

End Class

''' <summary>
'''感染症構造体
''' </summary>
''' <remarks>
''' </remarks>
Public Class typeKansensho
    <System.Xml.Serialization.XmlElement("KANSENSYO_NO")> _
    Public 感染症番号 As String

    <System.Xml.Serialization.XmlElement("KANSENSYO_NAME")> _
    Public 感染症名称 As String

    <System.Xml.Serialization.XmlElement("KANSENSYO_KBN")> _
    Public 感染症区分 As String

    <System.Xml.Serialization.XmlElement("KANSENSYO_KBN_NAME")> _
    Public 感染症区分名称 As String

End Class