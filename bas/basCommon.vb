﻿Imports System.Data.SqlClient

Module basCommon

    Public objConnect_LocalDB As New SqlConnection      'SQL Connection

    Public RET_NORMAL As Integer = 0                    '関数戻り値　正常 = 0
    Public RET_NOTFOUND As Integer = -1                 '関数戻り値　データ無 = -1
    Public RET_ERROR As Integer = -9                    '関数戻り値　エラー = -9
    Public RET_WARNING As Integer = -2                  '関数戻り値　ワーニング = -2

#Region "ログ出力"
    '*********************************************************************
    '* 関数名　　：subOutLog
    '* 関数概要　：所定フォルダログ出力
    '* 引　数　　：strMsg・・・出力メッセージ
    '* 　　　　　：intProc・・・メッセージ区分(0=Information、1=Error、2=Warning)
    '* 戻り値　　：無
    '* 作成者　　：Created By Watanabe 2010/03/19
    '* 更新履歴　：
    '*********************************************************************
    Public Sub subOutLog(ByVal strMsg As String, ByVal intProc As Integer)

        Dim path_LOG As String = String.Empty
        Dim strPath As String = String.Empty

        If My.Settings.LOG_PATH.EndsWith("\") Then
            path_LOG = My.Settings.LOG_PATH
        Else
            path_LOG = My.Settings.LOG_PATH + "\"
        End If

        '****************************************************
        '* ログ出力所定パス+ 年のフォルダが無ければ作成する *
        '****************************************************
        strPath = path_LOG & DateTime.Now.ToString("yyyy")
        If System.IO.Directory.Exists(strPath) = False Then
            System.IO.Directory.CreateDirectory(strPath)
        End If

        '*********************************************************
        '* ログ出力所定パス+ 年 + 月のフォルダが無ければ作成する *
        '*********************************************************
        strPath &= "\" & DateTime.Now.ToString("MM")
        If System.IO.Directory.Exists(strPath) = False Then
            System.IO.Directory.CreateDirectory(strPath)
        End If

        '**************************************************************
        '* ログ出力所定パス+ 年 + 月 + 日のフォルダが無ければ作成する *
        '**************************************************************
        'strPath &= "\" & DateTime.Now.ToString("dd")
        'If System.IO.File.Exists(strPath) = False Then
        '    System.IO.Directory.CreateDirectory(strPath)
        'End If

        Dim file_LOG As String = String.Empty

        file_LOG = strPath & "\" & _
                   DateTime.Now.ToString("yyyyMMdd") + ".log"

        Dim objsw As New System.IO.StreamWriter(file_LOG, _
                                                True, _
                                                System.Text.Encoding.GetEncoding(932))
        Dim strCate As String = ""

        Select Case intProc
            Case 0          'Information
                strCate = "[INF] " & DateTime.Now.ToString("yyyyMMdd HH:mm:ss") & " :"
            Case 1          'Error
                strCate = "[ERR] " & DateTime.Now.ToString("yyyyMMdd HH:mm:ss") & " :"
            Case 2          'Warning
                strCate = "[WAR] " & DateTime.Now.ToString("yyyyMMdd HH:mm:ss") & " :"
        End Select

        'Console.WriteLine(strCate & strMsg)
        'Console.ReadLine()

        objsw.WriteLine(strCate & strMsg)

        objsw.Close()
        objsw.Dispose()
    End Sub
#End Region

#Region "データファイルの移動"
    '*********************************************************************
    '* 関数名　　：subMoveFile
    '* 関数概要　：データファイルの処理後のファイル移動
    '* 引　数　　：strMsg・・・出力メッセージ
    '* 　　　　　：intProc・・・メッセージ区分(0=Information、1=Error、2=Warning)
    '* 戻り値　　：無
    '* 作成者　　：Created By Watanabe 2010/03/19
    '* 更新履歴　：
    '*********************************************************************
    Public Sub subMoveFile(ByVal strFileNM As String, ByRef strMsg As String, ByVal intProc As Integer)

        Dim path_LOG As String = String.Empty
        Dim strPath As String = String.Empty
        Dim strWorkPath As String = String.Empty

        If My.Settings.WORK_PATH.EndsWith("\") Then
            path_LOG = My.Settings.WORK_PATH
            strWorkPath = My.Settings.WORK_PATH
        Else
            path_LOG = My.Settings.WORK_PATH + "\"
            strWorkPath = My.Settings.WORK_PATH + "\"
        End If

        If intProc = 0 Then
            path_LOG &= "OK\"
        Else
            path_LOG &= "ERR\"
        End If
        '****************************************************
        '* ログ出力所定パス+ 年のフォルダが無ければ作成する *
        '****************************************************
        strPath = path_LOG & DateTime.Now.ToString("yyyy")
        If System.IO.Directory.Exists(strPath) = False Then
            System.IO.Directory.CreateDirectory(strPath)
        End If

        '*********************************************************
        '* ログ出力所定パス+ 年 + 月のフォルダが無ければ作成する *
        '*********************************************************
        strPath &= "\" & DateTime.Now.ToString("MM")
        If System.IO.Directory.Exists(strPath) = False Then
            System.IO.Directory.CreateDirectory(strPath)
        End If

        If System.IO.File.Exists(strPath & "\" & strFileNM) = False Then
            System.IO.File.Move(strWorkPath & strFileNM, strPath & "\" & strFileNM)
        Else
            Exit Sub
        End If
    End Sub
#End Region

#Region "SQLサーバConnection"
    '*********************************************************************
    '* 関数名　　：fncDBConnect
    '* 関数概要　：SQLサーバConnection
    '* 引　数　　：strMsg・・・メッセージ戻し用
    '* 戻り値　　：Boolean
    '* 作成者　　：Created By Watanabe 2010/03/19
    '* 更新履歴　：
    '*********************************************************************
    Public Function fncDBConnect(ByRef strMsg As String) As Boolean

        Try
            Dim connectionString As New System.Text.StringBuilder
            connectionString.Append("Data Source=")
            connectionString.Append(My.Settings.DATASOURCE)
            connectionString.Append(";")
            connectionString.Append("Initial Catalog=")
            connectionString.Append(My.Settings.CATALOG)
            connectionString.Append(";")
            connectionString.Append("UID=")
            connectionString.Append(My.Settings.DBUSER)
            connectionString.Append(";")
            connectionString.Append("PWD=")
            connectionString.Append(My.Settings.DBPASS)
            connectionString.Append(";")
            connectionString.Append("Integrated Security=false;")
            connectionString.Append("Connect Timeout = 30;")

            With objConnect_LocalDB
                If .State <> ConnectionState.Closed Then
                    .Close()
                End If
                .ConnectionString = connectionString.ToString
                .Open()
            End With

            Call subOutLog("fncDBConnect 正常終了", 0)

            Return True

        Catch ex As Exception

            strMsg = "fncDBConnect " & Space(1) & ex.Message
            Call subOutLog(strMsg, 1)
            Return False

        End Try

    End Function
#End Region

#Region "SQLサーバDisConnect"
    '*********************************************************************
    '* 関数名　　：fncDBDisConnect
    '* 関数概要　：SQLサーバDisConnect
    '* 引　数　　：strMsg・・・メッセージ戻し用
    '* 戻り値　　：Boolean
    '* 作成者　　：Created By Watanabe 2010/03/19
    '* 更新履歴　：
    '*********************************************************************
    Public Function fncDBDisConnect(ByRef strMsg As String) As Boolean

        Try

            If Not objConnect_LocalDB Is Nothing Then
                objConnect_LocalDB.Close()
                objConnect_LocalDB.Dispose()
            End If

            Call subOutLog("fncDBDisConnect 正常終了", 0)
            Return True
        Catch ex As Exception
            strMsg = "fncDBDisConnect " & Space(1) & ex.Message
            Call subOutLog(strMsg, 1)
            Return False
        End Try
    End Function
#End Region

#Region "SQL発行"
    '*********************************************************************
    '* 関数名　　：fncAdptSQL
    '* 関数概要　：SQL発行
    '* 引　数　　：objCon・・・Connection
    '* 　　　　　：strSQL・・・SQL文
    '* 　　　　　：strMsg・・・Errorメッセージ戻
    '* 戻り値　　：DataSet
    '* 作成者　　：Created By Watanabe 2010/03/19
    '* 更新履歴　：
    '*********************************************************************
    Public Function fncAdptSQL(ByRef objCon As SqlConnection, ByVal strSQL As String, ByVal strMsg As String) As DataSet

        fncAdptSQL = New DataSet

        Try
            Dim objAdpt As SqlDataAdapter = New SqlDataAdapter(strSQL, objCon)

            objAdpt.SelectCommand.CommandTimeout = 0
            objAdpt.Fill(fncAdptSQL)

            Call subOutLog("fncAdptSQL 正常終了", 0)

            objAdpt.Dispose()
            fncAdptSQL.Dispose()

        Catch ex As Exception

            strMsg = "fncAdptSQL" & Space(1) & ex.Message
            subOutLog(strMsg, 1)

        End Try

    End Function


    Public Function fncExecuteNonQuery(ByRef objCon As SqlConnection, ByVal strSQL As String, Optional ByVal strMsg As String = "") As Integer

        Try

            Dim objCommand As SqlCommand

            objCommand = objCon.CreateCommand()
            objCommand.CommandText = strSQL

            Dim iResult As Integer = objCommand.ExecuteNonQuery()
            objCommand.Dispose()

            'Call subOutLog("fncExecuteNonQuery 正常終了", 0)

            Return iResult

        Catch ex As Exception

            strMsg = "fncExecuteNonQuery" & Space(1) & ex.Message
            subOutLog(strMsg, 1)
            Return 0

        End Try

    End Function

#End Region

#Region "所定フォルダファイル名取得"
    '*********************************************************************
    '* 関数名　　：subGetAllFiles
    '* 関数概要　：所定フォルダのファイル名取得
    '* 引　数　　：strFolder ・・・対象フォルダPath
    '* 　　　　　：strPattern・・・取得ファイルのパターン
    '* 　　　　　：aryFiles　・・・ファイル名退避用
    '* 　　　　　：intProc　 ・・・処理フラグ(0=所定フォルダ内のみ、1=サブフォルダも含める)
    '* 戻り値　　：DataSet
    '* 作成者　　：Created By Watanabe 2010/03/19
    '* 更新履歴　：
    '*********************************************************************
    Public Sub subGetAllFiles(ByVal strFolder As String, _
        ByVal strPattern As String, ByRef aryFiles As ArrayList, ByVal intProc As Integer)

        '**********************************
        '* Folderにあるファイルを取得する *
        '**********************************
        Dim fs As String() = _
            System.IO.Directory.GetFiles(strFolder, strPattern)

        '***********************
        '* ArrayListに追加する *
        '***********************
        aryFiles.AddRange(fs)

        If intProc = 0 Then Exit Sub

        '**************************
        '* サブフォルダを取得する *
        '**************************
        Dim ds As String() = System.IO.Directory.GetDirectories(strFolder)

        '**************************************
        '* サブフォルダにあるファイルも調べる *
        '**************************************
        Dim d As String
        For Each d In ds
            subGetAllFiles(d, strPattern, aryFiles, 1)
        Next d

    End Sub
#End Region

#Region "fncGetServerDate サーバ日付取得"
    '************************************************************
    '* 関数名称　：fncGetServerDate
    '* 機能概要　：サーバ日付取得
    '* 引　数　　：intProc・・・0=日付取得
    '* 　　　　　：       　　　1=時刻取得
    '* 　　　　　：             2=日付＋時刻
    '* 　　　　　：strMsg・・・エラーメッセージ
    '* 戻り値　　：String・・・サーバ日付
    '* 　　　　　：      　　　intProcが2の場合 日付 & "," & 時刻
    '* 作成日付　：2010/10/08
    '* 作成者　　：Created By Watanabe
    '* 更新履歴　：
    '************************************************************
    Public Function fncGetServerDate(ByVal intProc As Integer, ByRef strMsg As String) As String
        Dim strDate As String = vbNullString
        Dim strSQL As New System.Text.StringBuilder
        Dim dsData As New DataSet

        Try
            '///SQL生成
            strSQL.Clear()

            If intProc = 0 Then
                strSQL.AppendLine("SELECT CONVERT(char(10), GETDATE(), 112) AS NOW_DATE ")
            ElseIf intProc = 1 Then
                strSQL.AppendLine("SELECT REPLACE(CONVERT(varchar,GETDATE(),108),':','') AS NOW_TIME")
            ElseIf intProc = 2 Then
                strSQL.AppendLine("SELECT ")
                strSQL.AppendLine(" CONVERT(char(10), GETDATE(), 112) AS NOW_DATE,")
                strSQL.AppendLine(" REPLACE(CONVERT(varchar,GETDATE(),108),':','') AS NOW_TIME ")

            End If

            '///SQL発行
            dsData = fncAdptSQL(objConnect_LocalDB, strSQL.ToString, strMsg)

            If intProc = 0 Then
                '///取得データを退避
                strDate = dsData.Tables(0).Rows(0).Item("NOW_DATE")
            ElseIf intProc = 1 Then
                '///取得データを退避
                strDate = dsData.Tables(0).Rows(0).Item("NOW_TIME")
            ElseIf intProc = 2 Then
                strDate = dsData.Tables(0).Rows(0).Item("NOW_DATE")
                strDate &= "," & dsData.Tables(0).Rows(0).Item("NOW_TIME")
            End If

            Return strDate

        Catch ex As Exception
            strMsg = ex.Message
            Return vbNullString
        End Try
    End Function
#End Region

End Module
